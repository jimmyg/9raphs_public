# Intro

This repository is a dowsized version of the code I created for an innovative product in image analysis. The prototype was developed from June 2017 to March 2019. I wrote the code on my own. Unfortunately the idea failed to materialize beyond a prototype.

## Purpose of the product

Reverse engineer images of statistical nature (line, column, pie,.. any chart) into a csv. A sort of an OCR for statistical images. 


## Repository content

### Alpha directory
Contains the last running version of the prototype.

### Scripts directory
Contains selected python scripts that were used to standardize the data and train the models in the prototype:
- [ChartYoloRun](scripts/ChartYoloRun.py): runs one image through the object detection model
- [CreateDarknetInputs](scripts/CreateDarknetInputs.py): script used to create the inputs to train the Yolov2 object detection algorithm on statistical images
- [DBUtils](scripts/DBUtils.py): python script containing supporting functions to the prototype or other related processes
- [CreateGraphCategorization-FitGenerator](scripts/CreateGraphCategorization-FitGenerator.ipynb): a neural net developed to categorize charts into groups (pie chart, line chart ...)

## High-level flowchart

<img src="images/9raphs.png" alt="high-level flowchart" width="600"/>

## Architecture
**Technology stack:** python, keras, scikit-learn, SQL, AWS, VBA

### Product Architecture
- Object-oriented implementation
- Several processes ran concurrently
- A master process managed the distribution of tasks to child processes 
- Each child focused on a specific tasks (for example scraping the web, downloading an image, classifying an image, decomposing an image)
- Processes communicated using pipes
- Each step was logged into a database running in the background

### Machine learning development
Several parts of the product required creation of a dataset and learning to feed neural nets. The code for that was separate from the implementation of the product.

**Object detection:** A major part of the process involves an object detection algorithm adapted to images of statistical nature. This was done using [yolo v2](https://pjreddie.com/darknet/yolov2/) (an object detection algorithm), which I re-trained specifically on these types of images. A large part of building this prototype went into this development. The resulting model could differentiate between the following objects in the image: 
- Title of the chart
- Other text boxes in the chart
- Axes (X, Y or Z) of the chart
- Chart legend
- Labels in the chart
- The main part of the chart

Other machine learning efforts evolved around classifying the images downloaded from the web as either 'of statistical nature' or not, and to categorize them per type (bar chart, line chart, column chart,...).

The dataset used as input for the training of the various neural nets and yolov2 was created using a large VBA script, creating a diverse set of several hundreds of thousands of images. Additional operations were done to align image size, colors, and other related inputs.

## Example of prototype outcome
The images below show the output of the image detection performed on statistical images downloaded from the web. The boxes in the images show the boundaries of the different objects that are detected (different colors for different elements):

<img src="images/Squares-2.png" alt="Example 1" width="320"/>
<img src="images/Squares-17.png" alt="Example 2" width="320"/>
<img src="images/Squares-20.png" alt="Example 3" width="320"/>


## Example of product outcome (if fully working)

Say you are looking for the revenues of company X. You launch a search, and the following image would be among the search results:

<img src="images/exampleChart.png" alt="example chart" width="400"/>

After download, and analysis, the process delivers the following results for that image:

||2016|2017|2018|2019|
|---|---|---|---|---|
|US|81|88|92|100|
|Europe|45|49|53|56|
|Greater China|42|48|55|60|
|ROW|12|17|22|25|
