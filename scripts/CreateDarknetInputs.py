 '''
This script is used to create the inputs for detection of objects
in the darknet package
It creates 2 types files:
    1. files related to charts with everything (incl pointcoords)
    2. files related to subcharts with pointcoords
 #Input from excel comes in the following order: [Left,Top,Width,Height,CenterLeft,CenterTop]
 #Output for Yolo should be: [Cat Nr,X_center,Y_center,Width,Height]
 
version control:
v2.1:
 - allows for creation of data based on "batchsize" which controls the number of files to be included in the h5 files
 - new fct to delete h5 files
 - y data is included in the h5 files
'''

import numpy as np
import pandas as pd
import ast
from shutil import copyfile
from PIL import Image
import os
from os.path import expanduser
#from sklearn.cluster import KMeans
#from skimage import io
from scipy.misc import imread, imsave
#from sklearn import preprocessing
from scipy import ndimage
#import h5py as h5
import os.path
#import cv2
import matplotlib.patches as patches
import matplotlib.pyplot as plt
%matplotlib inline

#YOLOv2 wants every dimension relative to the dimensions of the image
#[category number] [object center in X] [object center in Y] [object width in X] [object width in Y]
#Yolov2 Chart file
#0: title
#1: XCoords
#2: Y1Coords
#3: Legends
#4: XCaptions
#5: Y1Captions
#6: Labels
#7: Texts (source or subtitle doesn't matter)
# 'simple':8: Graph
# 'grouped': 8,9,10,11,12,13,14,15: depending on ChartTypeGroup
# 'detailed': any number 7 + x where x is the ChartTypeNr

#Yolov2 Subchart file:
#0: Point

#Yolov2 Label file:
#0: Label



def draw_coords (ax, plt, Coords,Color):
    '''
    Used to draw the rectanlges on an img
    '''
    #print(np.shape(Coords))
    #print("row:"+str(Coords[0])+","+str(Coords[1])+","+str(Coords[2])+","+str(Coords[3]))
    Rect = patches.Rectangle((Coords[0],Coords[1]),Coords[2],Coords[3],linewidth=4,edgecolor=Color, facecolor='none')
    ax.add_patch(Rect)

    return

def norm_data (X):
    '''
    Used to normalize the data with avg 0 and variance 1
    '''
    X = X/255
    mu = 1/(X.shape[0]*X.shape[1])*np.sum(X)
    X = X-mu
    sigma2 = 1/(X.shape[0]*X.shape[1])*np.sum(X**2)
    X /= sigma2
    X = X.astype(np.float32)
    return X

def rescale(x,start,stop):
    '''
    calculates new coordinates based on old coordinates and start / stop
    '''
    new_x = (x-start)/(stop-start)
    return new_x

def recreate_image(codebook, labels, w, h):
    '''
    Recreate the (compressed) image from the code book & labels
    '''
    d = codebook.shape[1]
    img = np.zeros((w, h, d))
    label_idx = 0
    for i in range(w):
        for j in range(h):
            img[i][j] = codebook[labels[label_idx]]
            label_idx += 1
    return img

def compress_image(img, n_colors):
    '''
    applies kmeans to an image and returns the simplified image back"
    '''
    img_array = np.array(img)
    w, h, d = tuple(img_array.shape)
    #print(img_array.shape)
    image_array = np.reshape(img_array, (w * h, d))
    kmeans = KMeans(n_clusters=n_colors).fit(image_array)
    labels = kmeans.predict(image_array)
    return recreate_image(kmeans.cluster_centers_, labels, w, h)


def deleteCharts(i,filetype):
    '''
    Deletes the charts  files created with the specificd [filetype]
    Usually used for txt files created for Yolov2
    '''
    StrCommand = "sudo rm -f "+expanduser("~")+"/jim/9raph/Data/Graphs/Chart"+str(i)+"*."+filetype
    os.system(StrCommand)

def deleteh5(i):
    '''
    Deletes h5 files
    '''
    StrCommand = "sudo rm -f "+expanduser("~")+"/jim/9raph/Data/Graphs_batch"+str(i)+"*.h5"
    os.system(StrCommand)
    
def deleteSubcharts(i,filetype):
    '''
    Deletes the subchart files created with the specificd [filetype]
    '''
    StrCommand = "sudo rm -f "+expanduser("~")+"/jim/9raph/Data/Subcharts/SubChart"+str(i)+"*."+filetype
    os.system(StrCommand)

def createYoloInputs(folder,data, graphClassification = 'simple'):
    '''
    This creates the inputs needed for Yolov2 algorithm
    This procedure only considers a "chart area" regardless of the type of chart
    #Input from excel comes in the following order: [Left,Top,Width,Height,CenterLeft,CenterTop]
    #Output for Yolo should be: [Cat Nr,X_center,Y_center,Width,Height]
    Output is:
        0: title
        1: XCoords
        2: Y1Coords
        3: Legends
        4: XCaptions
        5: Y1Captions
        6: Labels
        7: Texts (source or subtitle doesn't matter)
        8: Graph
    '''
    for index, row in data.iterrows():
        #Read and convert the coordinates from the csv file
        GraphArea=np.array(ast.literal_eval(row['PlotAreaCoords'].replace(";", ",")))
        SubTitleCoords = np.array(ast.literal_eval(row['SubtitleCoords'].replace(";", ",")))
        SourceCoords = np.array(ast.literal_eval(row['SourceCoords'].replace(";", ",")))
        Y1Coords=np.array(ast.literal_eval(row['Y1Coords'].replace(";", ",")))
        XCoords=np.array(ast.literal_eval(row['XCoords'].replace(";", ",")))
        Y1CaptionCoords=np.array(ast.literal_eval(row['Y1CaptionCoords'].replace(";", ",")))
        XCaptionCoords=np.array(ast.literal_eval(row['XCaptionCoords'].replace(";", ",")))
        TitleCoords=np.array(ast.literal_eval(row['TitleCoords'].replace(";", ",")))
        LegendCoords = np.array(ast.literal_eval(row['LegendCoords'].replace(";", ",")))
        LabelCoords = np.array(ast.literal_eval(row['DatalabelCoords'].replace(";", ",")))
        PointCoords = np.array(ast.literal_eval(row['PtCoords'].replace(";", ",")))
        #Y2Coords=np.array(ast.literal_eval(row['Y2Coords'].replace(";", ",")))
        #Y2CaptionCoords=np.array(ast.literal_eval(row['Y2CaptionCoords'].replace(";", ",")))
        #Create the coords for the subchart
        #As per the Excel macro, if one of the elements in the chart is missing, the value of its coords is [-1...]
        nrSer, nrCol, nrCoords = PointCoords.shape
        LabelCoords = np.reshape(LabelCoords,(nrSer*nrCol,nrCoords))
        #Write the txt files for the charts
        if graphClassification == 'simple':
            f = open(folder+"Graphs_simple/"+row['ChartName']+".txt" ,'w')
        elif graphClassification == 'grouped':
            f = open(folder+"Graphs_grouped/"+row['ChartName']+".txt",'w')
        else:
            f = open(folder+"Graphs_detailed/"+row['ChartName']+".txt" ,'w')
        if TitleCoords[0]>-0.01: f.write("0 "+str(TitleCoords[4])+" "+str(TitleCoords[5])+" "+str(TitleCoords[2])+" " +str(TitleCoords[3])+os.linesep)
        if XCoords[0]>-0.01: f.write("1 "+str(XCoords[4])+" "+str(XCoords[5])+" "+str(XCoords[2])+" " +str(XCoords[3])+os.linesep)
        if Y1Coords[0]>-0.01: f.write("2 "+str(Y1Coords[4])+" "+str(Y1Coords[5])+" "+str(Y1Coords[2])+" " +str(Y1Coords[3])+os.linesep)
        if LegendCoords[0]>-0.01: f.write("3 "+str(LegendCoords[4])+" "+str(LegendCoords[5])+" "+str(LegendCoords[2])+" " +str(LegendCoords[3])+os.linesep)
        if XCaptionCoords[0]>-0.01: f.write("4 "+str(XCaptionCoords[4])+" "+str(XCaptionCoords[5])+" "+str(XCaptionCoords[2])+" " +str(XCaptionCoords[3])+os.linesep)
        if Y1CaptionCoords[0]>-0.01: f.write("5 "+str(Y1CaptionCoords[4])+" "+str(Y1CaptionCoords[5])+" "+str(Y1CaptionCoords[2])+" " +str(Y1CaptionCoords[3])+os.linesep)
        if np.sum(LabelCoords[:,:]) > -LabelCoords.size:
            for i in LabelCoords:
                if (i[0]>-0.01): f.write("6 "+str(i[4])+" "+str(i[5])+" "+str(i[2])+" " +str(i[3])+os.linesep)
        if SubTitleCoords[0]>-0.01: f.write("7 "+str(SubTitleCoords[4])+" "+str(SubTitleCoords[5])+" "+str(SubTitleCoords[2])+" " +str(SubTitleCoords[3])+os.linesep)
        if SourceCoords[0]>-0.01: f.write("7 "+str(SourceCoords[4])+" "+str(SourceCoords[5])+" "+str(SourceCoords[2])+" " +str(SourceCoords[3])+os.linesep)
        if graphClassification == 'simple':
            f.write("8 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
        elif graphClassification == 'grouped':
            if row['ChartTypeGroup'] == 'Area': f.write("8 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
            elif row['ChartTypeGroup'] == 'Bar': f.write("9 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
            elif row['ChartTypeGroup'] == 'Column': f.write("10 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
            elif row['ChartTypeGroup'] == 'Doughnut': f.write("11 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
            elif row['ChartTypeGroup'] == 'Line': f.write("12 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
            elif row['ChartTypeGroup'] == 'Pie': f.write("13 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
            elif row['ChartTypeGroup'] == 'Radar': f.write("14 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
            #there shouldn't be any "else" unless there was a mistake
            else: f.write("15 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
        else:
            f.write(str(7+row['ChartTypeNr']) + " " +str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[2])+" " +str(GraphArea[3])+os.linesep)
        #if Y2Coords[0]>-0.01: f.write("8 "+str(Y2Coords[4])+" "+str(Y2Coords[5])+" "+str(Y2Coords[2])+" " +str(Y2Coords[            elif row['ChartTypeGroup'] == 'Area': f.write("8 "+str(GraphArea[4])+" "+str(GraphArea[5])+" "+str(GraphArea[3])+"\n")
        #if Y2CaptionCoords[0]>-0.01: f.write("9 "+str(Y2CaptionCoords[4])+" "+str(Y2CaptionCoords[5])+" "+str(Y2CaptionCoords[2])+" " +str(Y2CaptionCoords[3])+"\n")
        f.close()


def copySubchart(Originfolder,Destfolder,data,pct_inc,x_pix,y_pix, picformat):
    '''
    This creates the subcharts based on the charts and the PlotAreaCoords
    Note: Input from excel comes in the following order: [Left,Top,Width,Height,CenterLeft,CenterTop]
    '''
    #fscall= open (Destfolder + "Subcharts/SubALL.csv","w")
    #I Use fscall to quickly get a sense of the sizes of the subcharts
    for index, row in data.iterrows():
        GraphArea=np.array(ast.literal_eval(row['PlotAreaCoords'].replace(";", ",")))
        im=Image.open(Originfolder +"Graphs/" + row['ChartName']+picformat)
        im_w,im_h = im.size
        left = max(0,GraphArea[0]-pct_inc)
        top = max(0,GraphArea[1]-pct_inc)
        right = min(1,GraphArea[0]+GraphArea[2]+pct_inc)
        bottom = min(1,GraphArea[1]+GraphArea[3]+pct_inc)
        width = right - left
        height = bottom - top
        '''
        !!Cant remember why I have fsc
        fsc= open (Destfolder + "Subcharts/Sub"+row['ChartName']+"-Size.csv","w")
        fsc.write("datatype,l,t,r,b,w,h"+"\n")
        fsc.write("Graph_new,"+str(left)+","+str(top)+","+str(right)+","+str(bottom)+","+str(width)+","+str(height)+"\n")
        '''
        box = (int(left*im_w),int(top*im_h),int(right*im_w),int(bottom*im_h))
        im = im.crop(box)
        im = im.resize((x_pix,y_pix))
        #im2 = norm_data(im2)
        
        
        #IF image needs to be compressed:
        #Gridlines = bool(row['Y1HasGridlines'])
        #im_out = compress_image(np.array(im2),row['NrSeries']+Gridlines+1)
        #im_out = norm_data(im_out)
        
        '''
        This is where the subchart is saved, choose another picformat if needed
        '''
        
        imsave(Destfolder + "Subcharts/Sub"+row['ChartName']+ ".png",im)
        #im2.save(Destfolder + "Subcharts/Sub"+row['ChartName']+ "-AKM.bmp")
        #fsc.close()
        im.close()
    #fscall.close()

def copyjpgChart(Origfolder,Destfolder,data,picformat):
    '''   
    This copies the Charts in [data] from folder "[originfolder]/Graphs" to "[Destfolder]/Charts" 
    '''
    for index, row in data.iterrows():
        copyfile(Originfolder+"Graphs/"+row['ChartName']+picformat, Destfolder+"Charts/"+row['ChartName']+".jpg")


def load_data (folder,data,h,w,batchnr,picformat):
    '''
    This loads each image in [data] into a dataframe X
    '''
    print("load_date shape:",data.shape[0])
    X = np.zeros((data.shape[0],h+1,w,3))
    i = 0
    for _, row in data.iterrows():
        file = folder + "Sub" + row['ChartName'] + picformat
        image = np.array(ndimage.imread(os.path.join(folder,file), flatten=False))
        #print (file)
        #image = norm_data(image)
        X[i,0:h,:,:] = image[:,:,:]
        X[i,h,0,0] = int(row['ChartTypeNr'])
        #print("graph",s,"data=nrofseries:",int(data.loc[data['Graph']==s]['NrofSeries'].values==2))
        i+=1
    return X

def createh5X (folder,data,h,w,batchnr,sub_batchnr,picformat):
    '''
    Creates the h5 files in which load_data will load all the data
    '''

    filename = folder + "Graphs_batch"+str(batchnr)+"_"+str(sub_batchnr)+"_"+str(h)+"_"+str(w)+".h5"
    f = h5.File(filename, 'w')
    X = load_data(folder+"Subcharts/",data,h,w,batchnr,picformat)
    f.create_dataset("X", data = X)
    f.close()
    



def printChartAreas (folder, data, PrintOptions,picformat, maxPrints = 5 ):
    i = 0
    while i < maxPrints-1 and  i<data.shape[0]:
        #print ("txt file:" + folder+data.iloc[i,4]+".txt")
        df_areas = pd.read_csv(folder+data.iloc[i,4]+".txt", sep=" ", header=None)
        df_areas.columns = ["area_type", "X_center", "Y_Center", "Width", "Height"]
        img = Image.open(folder+data.iloc[i,4] + picformat)
        w,h = img.size
        fig, ax = plt.subplots(1)
        ax.imshow(img)
        #PA = data['PlotAreaCoords']
        #print(np.array(ast.literal_eval(PA.iloc[i].replace(";", ","))))
        #print(np.array(ast.literal_eval(data.ix[i,'Y1Coords'].replace(";", ","))))
        #print(np.array(ast.literal_eval(data.ix[i,'XCoords'].replace(";", ","))))
        for _, row in df_areas.iterrows():
            Coords = [float(row[1]-row[3]/2)*w,float(row[2]-row[4]/2)*h,float(row[3])*w,float(row[4])*h]
            if PrintOptions[int(row['area_type'])] == 1 and row['area_type'] == 0: draw_coords(ax,plt,Coords,'r')
            elif PrintOptions[int(row['area_type'])] == 1 and row['area_type'] == 1: draw_coords(ax,plt,Coords,'c')
            elif PrintOptions[int(row['area_type'])] == 1 and row['area_type'] == 2: draw_coords(ax,plt,Coords,'g')
            elif PrintOptions[int(row['area_type'])] == 1 and row['area_type'] == 3: draw_coords(ax,plt,Coords,'k')
            elif PrintOptions[int(row['area_type'])] == 1 and row['area_type'] == 4: draw_coords(ax,plt,Coords,'y')
            elif PrintOptions[int(row['area_type'])] == 1 and row['area_type'] == 5: draw_coords(ax,plt,Coords,'b')
            elif PrintOptions[int(row['area_type'])] == 1 and row['area_type'] == 6: draw_coords(ax,plt,Coords,'r')
            elif PrintOptions[int(row['area_type'])] == 1 and row['area_type'] == 7: draw_coords(ax,plt,Coords,'r')
            elif PrintOptions[int(row['area_type'])] == 1:  draw_coords(ax,plt,Coords,'m')
        plt.show()
        i+=1
        
    
    

pc_unix = "UNIX"
if pc_unix == "UNIX":
    home = expanduser("~")
    Originfolder = home +"/jim/9raphs_project/Data/"
    Destfolder = home +"/jim/9raphs_project/Data/"
else:
    Originfolder = "D:/9raphs_project/Data/"
    Destfolder = "D:/9raphs_project/Data/"

data = pd.read_csv(Originfolder+"Graphs.csv")[['BatchNr','ChartTypeNr',
        'ChartNr','ChartTypeGroup','ChartName','PlotAreaCoords','NrSeries','Y1HasGridlines',
        'Values','TitleCoords','Y1Coords','XCoords','LegendCoords',
        'DatalabelCoords','PtCoords','Y1CaptionCoords','XCaptionCoords',
        'SubtitleCoords','SourceCoords']]
#print(data.head(5))

pct_inc = 0.00 #percent to which we increase the size of the subchart we extract from the chart
x_pix = 240
y_pix = 240

picformat = ".jpg"
batchsize = 5000

start_batch = 1
#stop batch is excluded from loop
stop_batch = 2

'''
PrintOptions OPTIONS:
0: title
1: XCoords
2: Y1Coords
3: Legends
4: XCaptions
5: Y1Captions
6: Labels
7: Texts (source or subtitle doesn't matter)
8: Graph
'''
PrintOptions = [1,1,1,1,1,1,1,1,1]
j = 1

for i in range(start_batch,stop_batch):
    #print(i)

    data1 = data[(data['BatchNr'] == i)]
    #print("data1: ",data1.shape)
    #seems like in order to delete, I have to restart the kernel in Jupyter
    #the second argument for the deleteCharts function can be "txt","jpg", "csv" or "*"
    #deleteCharts(i,"txt")
    #deleteh5(i)
    #deleteSubcharts(i,"*")
    copySubchart(Originfolder,Destfolder,data1,pct_inc,x_pix,y_pix,picformat)
    
    #call YoloInputs with criteria of graphClassification either as
    #'simple','grouped' or 'detailed'
    #createYoloInputs(Destfolder,data1, 'grouped')
    
    #printChartAreas(Originfolder + 'Graphs_simple/',data1, PrintOptions , picformat)
    #copyjpgChart(Originfolder,Destfolder,data1)
    '''
    while (j * batchsize) < data1.shape[0]:
        data2 = data1[(data1['ChartNr'] > j*batchsize) & (data1['ChartNr'] <= min((j+1)*batchsize,data1.shape[0]))]
        print("h5: ",data2.shape," _ j: ",(j * batchsize))
        createh5X (Originfolder, data2, y_pix, x_pix, i,j,picformat)
        j+=1
        data2 = None
    '''
    j+=1
    data1 = None
    if j % 40 == 0: print()
    print('.', end='', flush=True)
data = None
print()
print ('done')
