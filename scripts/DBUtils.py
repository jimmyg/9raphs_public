#from google_images_download import google_images_download
import pandas as pd
import pymysql
import datetime
from PIL import Image
import os
import sys
import ast
import cv2
import numpy as np


def getExperimentNr (Originfolder):
    '''
    Returns the experiment number for a DL / ML network
    Each DL / ML experiment is logged on the drive with a unique ID and this function creates that ID
    '''
    filename = Originfolder + "Experiments.txt"
    nr = -1
    try:
        f = open(filename,'r')
    except:
        f = open(filename,'w')
        nr = 1
        f.write(str(nr))
        f.close
    else:
        nr = int(f.readlines()[-1])
        f.close()
        nr +=1
        f = open(filename,'w')
        f.write(str(nr))
        f.close()
    return nr


def deleteImage(ID, imgLoc, deleteFromDB = True, deleteFromImages = True):
    '''
    Deletes the image both from the DB and the drive containing images based on parameters received
    '''
    conn = MySQLDBConnect()
    #the DB should be (and currently is) set up in the way that the deletion of a Image entry, delete children
    if deleteFromDB:
        with conn.cursor() as cursor:
            sql = "DELETE FROM Image where Image.id = %s"
            cursor.execute(sql,(str(ID)))
        conn.commit()
    if deleteFromImages: os.remove(imgLoc)
    conn.close()


def openImage(filename):
    '''
    Opens an image only if it has the right extension
    filename: the path to the image to open
    Returns:
        result: a text that indicates the result of the operation
        img: the open image
    '''
    result = "ok"
    _, ext = os.path.splitext(filename)
    if ext == ".JPG" or ext == ".jpg" or ext == ".jpeg" or ext == ".JPEG" \
    or ext == ".PNG" or ext == ".png" or ext == ".bmp" or ext == ".BMP":
        try:
            img = Image.open(filename)
        except:
            result = "Can't open original img"
            img = 0
    else: 
        result = "Unknown format: " + ext
        img = 0
    return result, img


def GetUpdateOfSearchString():
    '''
    This function looks into the table SearchString to see if there is a new search to be executed
    If returns the next line in the table
    '''
    
    conn = MySQLDBConnect()
    with conn.cursor() as cursor:
        sql = "SELECT * FROM BatchSearchQueue WHERE Status = 'New' FOR UPDATE"
        cursor.execute(sql)
        rowcount = cursor.rowcount
        #print('rowcount: ' + str(rowcount))
        if rowcount == 0:
            NextSearch = [1,'Old','',0,'',False]
        else:
            NextSearch = cursor.fetchone()
        #print ('Nextsearch: ' + str(NextSearch))
        # update the SearchString
        sql = "UPDATE 9raphs.BatchSearchQueue SET status='In Progress' WHERE id=%s"
        cursor.execute(sql, (str(NextSearch[0])))
        conn.commit()
                
    conn.close()
    return NextSearch[0],NextSearch[2],NextSearch[3],NextSearch[4],NextSearch[5]


def MySQLDBConnect():
    '''
    This function establishes the connection to the AWS MySQL DB
    return: the connection
    '''
    host = "db9raphs.cexozeukxpsx.us-east-2.rds.amazonaws.com"
    port = 3306
    dbname = "9raphs"
    user = "jimmyg"
    password = "6QE$QIg1i!0i"
    conn = pymysql.connect(host, user=user, port=port, passwd=password, db=dbname)

    return conn

'''
def GoogleImgDownload_NewSearch(arguments, SearchID, logDirectory, searchMode = "Auto"):
'''
'''
    This function initiates the search with the googleImageDownload scraper
    This function creates a file in output which contians the terminal dump of the scraper
    That output can be used to feed out database
    :searchTerms: terms to be searched
    :searchID: Id in the batchsearchqueue table for updating the status
    :logdirectory: where to log the output of the process
    :searchmode: If:
        "Batch": that means that the status needs to indicate that the search is done, but the update to the DB will take place later (This is indicated with status 'SearchDone-noDBUpdate'
        "Auto" (default): meaning that this search is not performed in batch mode, hence the status will become 'SearchDone'
    :return: none
'''
'''
    response = google_images_download.googleimagesdownload()
    #response.download(arguments)
    f = open(logDirectory + arguments['keywords'] + ".out", 'w')
    sys.stdout = f
    response.download(arguments)
    print('FINISHED')
    conn = MySQLDBConnect()
    with conn.cursor() as cursor:
        if searchMode == "Batch":# update the SearchString
            sql = "UPDATE 9raphs.BatchSearchQueue SET status='SearchDone-noDBUpdate' WHERE id=%s"
        else: 
            sql = "UPDATE 9raphs.BatchSearchQueue SET status='SearchDone' WHERE id=%s"
        cursor.execute(sql, (str(SearchID)))
    conn.commit()
    conn.close()
    sys.stdout = sys.__stdout__

    #f.close()
'''

def GoogleImgDownload_OutFileToDataframe(filename):
    '''
    GoogleImgDownload_LogToDataframe converts the log file created by the google to a dataframe and a CSV file
    This function output a new file in csv format with the log info reformated to add to the dB
    :param filename: The name of the file to be converted to CSV
    :return: the dataframe containing what's written in the CSV file
    '''
    with open(filename) as f:
        data = f.readlines()
    dfGoogle_images = pd.DataFrame()
    folder = ''
    #check the status to make sure we hae 3 elements
    for n, line in enumerate(data, 1):
        ImageDetails = {}
        if line.strip()[0:9] == "Item no.:":
            folder = line.strip()[line.strip().find('=') + 2:]
        elif line.strip()[0:9] == "Now Downl":
            folder = line.strip()[line.strip().find('-') + 2:]
        elif line.strip()[0:9] == "Image Met":
            ImageDetails['filename'] = ''
            try:
                dict = ast.literal_eval(line.strip()[line.strip().find(':') + 2:])
            except:
                error = True
            else:
                error = False
            #This assumes that the only error can come from literal_eval
            #If there are errors with folder or imagename, then we will have more bugs
            #So far no bugs in file or folder
        elif line.strip()[0:21] == "Completed Image ====>":
            #print(dfGoogle_images.shape[0])
            ImageDetails['folder'] = folder
            ImageDetails['error'] = error
            dfdict = pd.DataFrame([dict], columns=dict.keys())
            ImageDetails['filename'] = line.strip()[line.strip().find('>') + 2:]
            #print(dfGoogle_images.ix[-1,'folder'])
            dfImg = pd.DataFrame([ImageDetails], columns=ImageDetails.keys())
            dfGoogle_images = pd.concat([dfGoogle_images, pd.concat([dfImg,dfdict],axis=1)], axis=0).reset_index(drop=True)
    dfGoogle_images.to_csv(filename+".csv", encoding='utf-8', index=False, header=True)
    #print (dfGoogle_images)
    return dfGoogle_images


def DBAddSearchResults(ImageDirectory,SearchResultsDF):
    '''
    DBAddSearchResults takes the dataframe containing all the search results from a scraping and
    adds the data in the dataframe to the DB
    '''

    previous_folder = ''


    for _,line in SearchResultsDF.iterrows():
        if line['error']:
            try:
                os.remove(ImageDirectory + line['folder'] + '/' + line['filename'])
            except:
                print("Unexpected error: wrong metadata and image not deleted - ", sys.exc_info()[0])
        else:
            conn = MySQLDBConnect()
            # Check if the image already exists in Im
            # Could also use SELECT EXISTS to do this check, it is a bit faster, but for now, whatevs
            if previous_folder == line['folder']:
                pass
            else:
                with conn.cursor() as cursor:
                    # Create a new record
                    sql = "INSERT INTO Search (SearchString, SearchDateTime, SearchStatus) " \
                          "VALUES (%s, %s, %s)"
                    cursor.execute(sql, (line['folder'], str(datetime.datetime.now())[0:19], 0))
                    last_Search_id = cursor.lastrowid
                    print ("new search line: " + line['folder'] + " - " + str(last_Search_id) )
                    #Delete files as we don't need them
                conn.commit()
                previous_folder = line['folder']
            with conn.cursor() as cursor:
                sql = "SELECT id FROM Image WHERE GglImgURL = %s"
                cursor.execute(sql,str(line['image_link']))
                rowcount = cursor.rowcount
                if rowcount == 0:
                    pass
                    isDuplicate = False
                else:
                    last_Image_id = cursor.fetchone()[0]
                    isDuplicate = True
            if rowcount == 0:
                #if the image didn't exist previously, insert it and get that row's id
                #First check if the image can be opened and that the file is not corrupt
                try:
                    Image.open(ImageDirectory + line['folder'] + '/'+ line['filename'])
                except:
                    try:
                        os.remove(ImageDirectory + line['folder'] + '/'+ line['filename'])
                    except:
                        print("Unexpected error: unreadable image not deleted - ", sys.exc_info()[0])
                    else:
                        pass
                        #print("Unreadable image deleted: " + str(ImageDirectory + line['folder'] + '/' + str(line['filename'])))
                else:
                    with conn.cursor() as cursor:
                        sql = "INSERT INTO Image (GglDescription, GglFileFormat, ImgHeight, GglImgHostDomain" \
                              ", GglImgURL, GglImgSource, GglImgThumbnailURL, ImgWidth, Folder, Filename, isTrainImg) " \
                              "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, FALSE)"
                        cursor.execute(sql, (line['image_description'],line['image_format'],line['image_height'],
                                             line['image_host'], line['image_link'],line['image_source'],
                                             line['image_thumbnail_url'], line['image_width'], line['folder'],
                                             str(line['filename'])))
                        last_Image_id = cursor.lastrowid
                    conn.commit()
                    with conn.cursor() as cursor:
                        sql = "INSERT INTO SearchResult (idSearch, idImage) VALUES (%s, %s)"
                        cursor.execute(sql, (str(last_Search_id), str(last_Image_id)))
                    conn.commit()
            else:
                #if it exists, link to previous instance of the image
                #Cannot remove the file as it causes to crash when the filename is really long
                try:
                    os.remove(ImageDirectory + line['folder'] + '/' + line['filename'])
                except:
                    print("Unexpected error: Existing image not deleted - ", sys.exc_info()[0])
                else:
                    pass
                    #print("Image already exists. Deleted: "+ line['filename'])
                with conn.cursor() as cursor:
                    sql = "INSERT INTO SearchResult (idSearch, idImage) VALUES (%s, %s)"
                    cursor.execute(sql, (str(last_Search_id), str(last_Image_id)))
                conn.commit()
            conn.close()


def resizeImg(source,h,w,destFolder,filename):
    '''
    Resizes an image "Source" into a destination folder
    :param Source: source filename of the file
    :param h: target height of the image
    :param w: target wdth of the image
    :param destfolder: destination folder of the image
    :return: result: status of the result
    '''
    _, ext = os.path.splitext(source)
    destinationFilename = destFolder + filename + ".png"
    result = "not ok"
    if not os.path.isfile(destinationFilename):
        result, img = openImage(source)
        if result == "ok":
            if ext == ".JPG" or ext == ".jpg" or ext == ".JPEG" or ext == ".jpeg":
                new_img = img.resize((w,h)).convert('RGBA')
                try:
                    new_img.save(destinationFilename, "PNG", optimize=True)
                except:
                    result = "can't convert from " + ext
            elif ext == ".PNG" or ext == ".png":
                new_img = img.resize((w,h)).convert('RGBA')
                try:
                    new_img.save(destinationFilename, "PNG", optimize=True)
                except:
                    result = "can't convert from " + ext
            elif ext == ".bmp" or ext == ".BMP":
                result = "no code for bmp yet "
                #need to create the code for BMP
            else: 
                result = "no code for other file format yet"
    else:
        result = "ok"
    try:
        X = Image.open(destinationFilename).convert('RGBA')
    except:
        result = "can't open resized img"
    else:
        result = "ok"
    return result


def createPNGImagesFromDB(sourceFolder, destFolder,h,w, flagDatasetIsBalanced = True, limit = 10000):
    '''
    Creates PNG files from imfo contained in the DB
    Params:
    sourceFolder: Folder containing the input images
    destFolder: Folder where the PNG images will be outpit
    flagDatasetIsBalanced:
        if True: create as many Graphs as NonGraphs (to avoid biased dataset)
        if False: Use all the images that are in the DB (regardless of difference in Graphs and NonGraphs files)
    limit: Indicates the max nr of images that are created
    '''
    if not os.path.isdir(destFolder):
    #Create the directories and the files
        os.mkdir(destFolder)
    
    #Figure out nr of images that will be created based on avialable info in DB, limit and flagDatasetIsBalanced
    conn = MySQLDBConnect()
    with conn.cursor() as cursor:
        sql = "SELECT count(*) from Image WHERE isGraphHuman = 'y'"
        cursor.execute(sql)
        conn.commit()
        nrOfGraphs = cursor.fetchone()
        sql = "SELECT count(*) from Image WHERE isGraphHuman = 'n'"
        cursor.execute(sql)
        conn.commit()
        nrOfNonGraphs = cursor.fetchone()
    conn.close()
    if flagDatasetIsBalanced:
        limit = min(nrOfNonGraphs,nrOfGraphs)
    
    #First deal with the images that are graphs
    conn = MySQLDBConnect()
    
    df = pd.read_sql("Select ID, Folder, Filename from Image where isGraphHuman is not null and isGraphHuman != 'c' order by ID limit " + str(limit), con=conn)
    conn.close()
    for _,line in df.iterrows():
        #print (df.shape[0])
        source = sourceFolder + str(line[1]) + '/' + str(line[2])
        #_,tail = os.path.split(source)
        #print(tail)
        if resizeImg(source,h,w,destFolder,str(line[0])) != "ok":
            deleteImage(line[0],source)


def kMeansImage(imgName, imgSource, destFolder, nrColors = 20):
    '''
    Applies kMeans to an image
    :param imgName: name of the output file
    :param imgSource: source file
    :param destFolder: folder where the image will be saved
    :param nrColors: nr of colors to apply kmeans
    :return: none
    '''
    image_orig = cv2.imread(imgSource)

    # apply Kmeans
    # convert to the right format np.float32
    Z = image_orig.reshape((-1, 3))
    Z = np.float32(Z)

    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = nrColors
    ret, label, center = cv2.kmeans(Z, K, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

    # Now convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    image_orig = res.reshape((image_orig.shape))
    cv2.imwrite(destFolder + imgName + ".png", image_orig)
    image_orig = None
