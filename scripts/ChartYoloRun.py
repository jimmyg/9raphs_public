'''
This file takes a chart through Yolo and draws the output

'''



from os.path import expanduser
import subprocess, shlex
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.patches as patches
get_ipython().magic('matplotlib inline')
from keras.models import Model, load_model
from sklearn.cluster import KMeans
import cv2

def draw_coords (ax, plt, height, width, Coords,Color):
    Rect = patches.Rectangle((Coords[0],Coords[1]),(Coords[2]-Coords[0]),(Coords[3]-Coords[1]),linewidth=4,edgecolor=Color, facecolor='none')
    ax.add_patch(Rect)
    cent_w = (Coords[2]+Coords[0])/ 2
    cent_h = (Coords[3]+Coords[1])/ 2
    plt.scatter(cent_w,cent_h, s=20, c=Color, marker='o')
    return

def recreate_image(codebook, labels, w, h):
    """Recreate the (compressed) image from the code book & labels"""
    d = codebook.shape[1]
    img = np.zeros((w, h, d))
    label_idx = 0
    for i in range(w):
        for j in range(h):
            img[i][j] = codebook[labels[label_idx]]
            label_idx += 1
    return img

def compress_image(img, n_colors):
    img_array = np.array(img)
    w, h, d = tuple(img_array.shape)
    #print(img_array.shape)
    image_array = np.reshape(img_array, (w * h, d))
    kmeans = KMeans(n_clusters=n_colors).fit(image_array)
    labels = kmeans.predict(image_array)
    return recreate_image(kmeans.cluster_centers_, labels, w, h)



home = expanduser("~")
Originfolder = home +"/jim/9raph/Data/"
chartfolder = Originfolder +"Graphs/"
outputfile = Originfolder +"result.txt"

Chart = input('Please enter Chart Nr (batch-nr):')

filename = chartfolder + "Chart" + Chart + ".jpg"

darknetfolder = home + "/jim/9raph/darknet/"
bashCommand = "./darknet detector test cfg/Charts.data cfg/yolo-Charts6.cfg ./backup/yolo-Charts6_6000.weights " + filename +" > " + outputfile
subprocess.call(bashCommand, shell = True, cwd = darknetfolder)

data = []
f = open(outputfile,"r")
idx = 0
for line in f:
    if "class" in line: 
        line = line.replace("%", "")
        #print(line)
        words = (line.split(","))
        for word in words:
            if not(word == 'class' or word == '\n'):
                data.append(int(word))
    idx += 1
f.close()
data = np.array(data)
data= data.reshape((int(len(data)/6),6))
#print(data)

with open(darknetfolder+"cfg/Charts.names") as f:
    content = f.read()
labels = content.split()
#print(labels)

img = Image.open(chartfolder + "Chart" + Chart + ".jpg")
width, height = img.size
fig, ax = plt.subplots(1)
ax.imshow(img)
#print(width, height)

for row in data:
    if row[0] == 0:
        draw_coords(ax,plt,height,width,row[2:],'r')
    elif row[0] == 1:
        draw_coords(ax,plt,height,width,row[2:],'c')
    elif row[0] == 2:
        draw_coords(ax,plt,height,width,row[2:],'g')
    elif row[0] == 3:
        draw_coords(ax,plt,height,width,row[2:],'k')
    elif row[0] == 4:
        draw_coords(ax,plt,height,width,row[2:],'y')
    elif row[0] == 5:
        draw_coords(ax,plt,height,width,row[2:],'b')
    elif row[0] == 6:
        draw_coords(ax,plt,height,width,row[2:],'l')
    else:
        draw_coords(ax,plt,height,width,row[2:],'m')

plt.show()

ExperimentNr = 222
h = 192
w = 224
input_img = img.resize((w,h))
output_img = compress_image(np.array(input_img),3)
X = np.zeros((1,h,w,3))
X [0,:,:,:] = output_img
#loaded_model = load_model(Originfolder +"/Models/Model12pts" + str(ExperimentNr) + ".h5")
#y = loaded_model.predict(X)
#print(y)
