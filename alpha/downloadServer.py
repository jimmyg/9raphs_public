from multiprocessing.managers import BaseManager
import sys
import time
from urllib.request import Request, urlopen
import http.client
import os
import json
import pandas as pd
from Utils import imageSearch
import hashlib
from bson.objectid import ObjectId
import datetime
from PIL import Image
from shutil import copyfile

http.client._MAXHEADERS = 1000

class downloadServer():

    def __init__(self,ip,port,authkey):
        super(downloadServer, self).__init__()
        self.ip = ip
        self.port = port
        self.authkey = authkey

        #Create the queus and variables
        BaseManager.register('q_sync')
        BaseManager.register('q_sql')
        BaseManager.register('q_search')
        BaseManager.register('q_download')
        BaseManager.register('q_darknet')
        BaseManager.register('q_imgProc')
        BaseManager.register('config')
        m = BaseManager(address=(ip, port), authkey=authkey)
        m.connect()
        self.q_download = m.q_download()
        self.q_sql = m.q_sql()
        self.q_sync = m.q_sync()
        self.q_search = m.q_search()
        self.q_darknet = m.q_darknet()
        self.q_imgProc = m.q_imgProc()
        self.config = m.config()._getvalue()
        self.q_sync.put(['Register',os.getpid(),'downloadServer'])

    def download_page(self,url):
        try:
            headers = {}
            headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
            req = Request(url, headers=headers)
            resp = urlopen(req)
            return resp
        except Exception as e:
            return 'error: /n' + str(e)

    def createURLHash(self,url):
        return ObjectId(hashlib.shake_128(str(url).encode('utf-8')).digest(12))

    # Download Page for more than 100 images
    def download_extended_page(self,url,chromedriver):
        from selenium import webdriver
        from selenium.webdriver.common.keys import Keys
        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
        options.add_argument("--headless")

        try:
            browser = webdriver.Chrome(chromedriver, chrome_options=options)
        except Exception as e:
            print("Looks like we cannot locate the path the 'chromedriver' (use the '--chromedriver' "
                  "argument to specify the path to the executable.) or google chrome browser is not "
                  "installed on your machine (exception: %s)" % e)
            sys.exit()
        browser.set_window_size(1024, 768)
        #browser.execute_script("document.body.style.zoom='67%'")

        # Open the link
        browser.get(url)
        time.sleep(0.5)
        element = browser.find_element_by_tag_name("body")
        # Scroll down
        for i in range(10):
            element.send_keys(Keys.PAGE_DOWN)
            time.sleep(0.1)
        try:
            browser.find_element_by_id("smb").click()
            for i in range(20):
                element.send_keys(Keys.PAGE_DOWN)
                time.sleep(0.3)  # bot id protection
        except:
            for i in range(5):
                element.send_keys(Keys.PAGE_DOWN)
                time.sleep(0.1)  # bot id protection

        time.sleep(0.1)

        source = browser.page_source #page source
        #close the browser
        browser.close()

        return source

    def createImageDict(self,filename):
        dfImages = pd.read_json(filename)
        dfImages.columns = ['ImageDescription', 'ImageFormat', 'ImageHeight', 'ImageHostDomain', 'ImageURL', 'ImageSource', 'ImageThumbnailURL', 'ImageWidth']
        dfImages['ImageURLHash'] = ""
        dfImages['ImageFilename'] = ""
        for index, line in dfImages.iterrows():
            dfImages.loc[index,'ImageURLHash'] = str(self.createURLHash(line['ImageURL']))
            dfImages.loc[index,'ImageFilename'] = str('Original.' + line['ImageFormat'])
            #print("URLHash: " + str(line['ImageURLHash']))
        dictImages = dfImages.to_dict('records')
        return dictImages


    def downloadPage(self, rcvd):
        argument = rcvd[1]
        ids = rcvd[2]
        error = 0
        errormsg = 'dowloadserver - downloadpage :'
        if argument['limit'] < 101:
            try:
                raw_html = self.download_page(argument['url'])  # download page
                resp = str(raw_html.read())
            except Exception as e:
                error = 1
                errormsg = errormsg + "Could not download page for : " + str(ids[1]) + " " + e
        else:
            try:
                resp = self.download_extended_page(argument['url'], argument['chromedriver'])
            except Exception as e:
                error = 1
                errormsg = errormsg +  errormsg + "Could not download extended page for : " + str(ids[1]) + " " + e

        filename = argument['htmlfile']
        f = open(filename, "w")
        f.write(resp)
        f.close()
        filename = argument['jsonfile']
        items = imageSearch.get_all_items(resp, argument['limit'])
        if len(items) == 0:
            error = 1
            errormsg = errormsg + "no items in the page " + str(argument['htmlfile']) + " IDS: " + str(ids)
        else:
            f = open(filename, "w")
            json.dump(items, f, indent=4, sort_keys=True)
            f.close()
            if argument['main'] == 'main':
                self.q_search.put(['downloadRelatedPage',ids,argument['searchString'],
                                   argument['htmlfile'],argument['keywords']])
            dictImages = self.createImageDict(filename)
            self.q_sql.put(['updateRelatedSearch', ids, dictImages])
        if error:
            self.q_sync.put(['Error', errormsg])



    def downloadImage(self, rcvd):
        ids = rcvd[1]
        URL = rcvd[2]
        filename = rcvd[3]
        efsFilename = rcvd[4]
        error = 0
        try:
            req = Request(URL, headers={
                "User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"})
            response = urlopen(req, None, 10)
            data = response.read()
            response.close()
            output_file = open(filename, 'wb')
            output_file.write(data)
            output_file.close()
            copyfile(filename, efsFilename)
        except Exception as e:
            error = 1 #error is dealt with in the code of updateImageDownloaded
        self.q_sql.put(['updateImageDownloaded',ids, error])

if __name__ == "__main__":
    if len(sys.argv) < 3:
        ip = '127.0.0.1'
        port = 24487
        authkey = 'loko3fvppokse556'.encode('utf-8')
        debug = False
    else:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        authkey = sys.argv[3].encode('utf-8')
        if len(sys.argv) == 5:
            debug = True
        else: debug = False
    downloadServer = downloadServer(ip, port, authkey)

    if debug:
        timestart = datetime.datetime.now()
        f = open('./debug/downloadServer - '  + str(os.getpid()) + '-' + str(timestart) + '.txt', 'w')

    while True:
        rcvd = downloadServer.q_download.get()
        if debug:
            from random import randint
            timestart = datetime.datetime.now()
            randomnr = randint(0,100000)
            f.write('downloadServer,Start,' + str(randomnr) + ',' + str(timestart) + ',' + str(os.getpid()) + ',' + str(rcvd) + '\n')
        if rcvd[0] == 'downloadPage':
            downloadServer.downloadPage(rcvd)
        elif rcvd[0] == 'downloadImage':
            downloadServer.downloadImage(rcvd)
        elif rcvd[0] == 'Close':
            if debug: f.close()
            break
        if debug:
            timestop = datetime.datetime.now()
            f.write('downloadServer,Done,' + str(randomnr) + ',' + str(timestop) + ',' + str(os.getpid()) + ',' + str(rcvd[1]) + ',' + str(timestop - timestart) + '\n')
