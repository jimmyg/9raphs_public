#!/usr/bin/env python
# In[ ]:
#  coding: utf-8

'''
Inspired by code from https://github.com/hardikvasa/google-images-download

This Utils file contains all the functions needed to initiate the imageSearch
None of the fcts here require the objects used in syncManager so it can be separated for better readability

'''
import os
import json
import sys
import re


# Finding 'Next tab' from the given raw page
def get_next_tab(s):
    start_line = s.find('class="dtviD"')
    if start_line == -1:  # If no links are found then give an error!
        end_quote = 0
        link = "no_tabs"
        return link, '', end_quote
    else:
        start_line = s.find('class="dtviD"')
        start_content = s.find('href="', start_line + 1)
        end_content = s.find('">', start_content + 1)
        url_item = "https://www.google.com" + str(s[start_content + 6:end_content])
        url_item = url_item.replace('&amp;', '&')

        s = s.replace('&amp;', '&')
        start_line_2 = s.find('class="dtviD">') + len('class="dtviD">')
        end_line_2 = s.find('</span>', start_line_2 + 1)
        url_item_name = str(s[start_line_2 :end_line_2])
        if url_item_name[-3:] == '%3D':
            end_content_3 = url_item_name.rfind(':')
            url_item_name = url_item_name[:end_content_3]
        url_item_name = url_item_name.replace(',g_1:', ' ')
        url_item_name = url_item_name.replace(',online_chips:', ' ')
        url_item_name = url_item_name.replace('+', ' ')
        return url_item, url_item_name, end_content


# Getting all links with the help of '_images_get_next_image'
def get_all_tabs(page, num_tabs):
    tabs = {}
    for i in range (num_tabs):
        item, item_name, end_content = get_next_tab(page)
        if item == "no_tabs":
            break
        else:
            tabs[item_name] = item  # Append all the links in the list named 'Links'
            # time.sleep(0.1)  # Timer could be used to slow down the request for image downloads
            page = page[end_content:]
    return tabs


#Creating the different argument {} for the download instructions and beyond
def createArguments(searchString, keywords):


    args_list = ["keywords", "limit", "format", "usage_rights", "size",
                 "time_range", "proxy", "specific_site",  "socket_timeout",
                 "language", "chromedriver", "safe_search", "url",
                 "htmlfile","jsonfile","searchString","main"]


    argument = {"searchString": searchString,
                 "chromedriver": "/usr/lib/chromium-browser/chromedriver"}

    ######Initialization and Validation of user arguments
    '''
    If you simply type the keyword, Google will best try to match it
    If you want to search for exact phrase, you can wrap the keywords in double quotes ("")
    If you want to search to contain either of the words provided, use OR between the words.
    If you want to explicitly not want a specific word use a minus sign before the word (-)
    '''

    argument['keywords'] = keywords
    #Create a almost empty list of arguments (missing values will be 'None' for now)
    for arg in args_list:
        if arg not in argument:
            argument[arg] = None


    #time_range parameter should be in the following format '{"time_min":"MM/DD/YYYY","time_max":"MM/DD/YYYY"}'
    if argument['time_range']:
        pass
        #This will allow to simplify search
        #need to look in db if this search already exists
        #if it does, only need to look from the date since that last search was performed

    # Proxy settings
    #You can specify the proxy settings in 'IP:Port' format
    if argument['proxy']:
        os.environ["http_proxy"] = argument['proxy']
        os.environ["https_proxy"] = argument['proxy']
        ######Initialization Complete

    #arguments['socket_timeout']
    #Allows you to specify the time to wait for socket connection.
    #You could specify a higher timeout time for slow internet connection. The default value is 10 seconds.

    #arguments['language']
    #Defines the language filter.The search results are automatically returned in that language
    #Possible Values: Arabic, Chinese(Simplified), Chinese(Traditional), Czech, Danish, Dutch,
    #English, Estonian.Finnish, French, German, Greek, Hebrew, Hungarian, Icelandic, Italian,
    #Japanese, Korean, Latvianm, Lithuanian, Norwegian, Portuguese, Polish, Romanian, Russian, Spanish, Swedish, Turkish

    return argument


#Building URL parameters
def build_url_parameters(argument):

    if argument['time_range']:
        json_acceptable_string = argument['time_range'].replace("'", "\"")
        d = json.loads(json_acceptable_string)
        time_range = ',cdr:1,cd_min:' + d['time_min'] + ',cd_max:' + d['time_min']
    else:
        time_range = ''

    if argument['language']:
        lang = "&lr="
        lang_param = {"Arabic":"lang_ar","Chinese (Simplified)":"lang_zh-CN","Chinese (Traditional)":"lang_zh-TW","Czech":"lang_cs","Danish":"lang_da","Dutch":"lang_nl","English":"lang_en","Estonian":"lang_et","Finnish":"lang_fi","French":"lang_fr","German":"lang_de","Greek":"lang_el","Hebrew":"lang_iw ","Hungarian":"lang_hu","Icelandic":"lang_is","Italian":"lang_it","Japanese":"lang_ja","Korean":"lang_ko","Latvian":"lang_lv","Lithuanian":"lang_lt","Norwegian":"lang_no","Portuguese":"lang_pt","Polish":"lang_pl","Romanian":"lang_ro","Russian":"lang_ru","Spanish":"lang_es","Swedish":"lang_sv","Turkish":"lang_tr"}
        lang_url = lang+lang_param[argument['language']]
    else:
        lang_url = ''
    built_url = "&tbs="
    counter = 0
    params = {'usage_rights':[argument['usage_rights'],
                              {'labeled-for-reuse-with-modifications':'sur:fmc','labeled-for-reuse':'sur:fc',
                               'labeled-for-noncommercial-reuse-with-modification':'sur:fm',
                               'labeled-for-nocommercial-reuse':'sur:f'}],
              'size':[argument['size'],
                      {'large':'isz:l','medium':'isz:m','icon':'isz:i','>400*300':'isz:lt,islt:qsvga',
                       '>640*480':'isz:lt,islt:vga','>800*600':'isz:lt,islt:svga','>1024*768':'visz:lt,islt:xga',
                       '>2MP':'isz:lt,islt:2mp','>4MP':'isz:lt,islt:4mp','>6MP':'isz:lt,islt:6mp',
                       '>8MP':'isz:lt,islt:8mp','>10MP':'isz:lt,islt:10mp','>12MP':'isz:lt,islt:12mp',
                       '>15MP':'isz:lt,islt:15mp','>20MP':'isz:lt,islt:20mp','>40MP':'isz:lt,islt:40mp',
                       '>70MP':'isz:lt,islt:70mp'}]}

    for key, value in params.items():
        if value[0] is not None:
            ext_param = value[1][value[0]]
            # counter will tell if it is first param added or not
            if counter == 0:
                # add it to the built url
                built_url = built_url + ext_param
                counter += 1
            else:
                built_url = built_url + ',' + ext_param
                counter += 1
    built_url = lang_url+built_url+time_range
    return built_url


def build_search_url(search_term,params,specific_site,safe_search):
    from urllib.parse import quote
    # check safe_search
    safe_search_string = "&safe=active"
    # check the args and choose the URL
    if specific_site:
        url = 'https://www.google.com/search?q=' + quote(
            search_term) + '&as_sitesearch=' + specific_site + '&espv=2&biw=1366&bih=667&site=webhp&source=lnms&tbm=isch' + params + '&sa=X&ei=XosDVaCXD8TasATItgE&ved=0CAcQ_AUoAg'
    else:
        url = 'https://www.google.com/search?q=' + quote(
            search_term) + '&espv=2&biw=1366&bih=667&site=webhp&source=lnms&tbm=isch' + params + '&sa=X&ei=XosDVaCXD8TasATItgE&ved=0CAcQ_AUoAg'

    # safe search check
    if safe_search:
        url = url + safe_search_string

    # print(url)
    return url

def replace_with_byte(match):
    return chr(int(match.group(0)[1:], 8))

def repair(brokenjson):
    invalid_escape = re.compile(r'\\[0-7]{1,3}')  # up to 3 digits for byte values up to FF
    return invalid_escape.sub(replace_with_byte, brokenjson)

def format_object(object):
    formatted_object = {}
    formatted_object['image_format'] = object['ity']
    formatted_object['image_height'] = object['oh']
    formatted_object['image_width'] = object['ow']
    formatted_object['image_link'] = object['ou']
    formatted_object['image_description'] = object['pt']
    formatted_object['image_host'] = object['rh']
    formatted_object['image_source'] = object['ru']
    formatted_object['image_thumbnail_url'] = object['tu']
    return formatted_object


def _get_next_item(s):
    start_line = s.find('rg_meta notranslate')
    if start_line == -1:  # If no links are found then give an error!
        end_quote = 0
        link = "no_links"
        return link, end_quote
    else:
        start_line = s.find('class="rg_meta notranslate">')
        start_object = s.find('{', start_line + 1)
        end_object = s.find('</div>', start_object + 1)
        object_raw = str(s[start_object:end_object])
        try:
            object_decode = bytes(object_raw, "utf-8").decode("unicode_escape")
            final_object = json.loads(object_decode)
        except:
            final_object = ""
        return final_object, end_object

def get_all_items(page,limit):
    items = []
    i = 0
    count = 1
    while count < limit+1:
        object, end_content = _get_next_item(page)
        if object == "no_links":
            break
        elif object == "":
            page = page[end_content:]
        else:
            #format the item for readability
            object = format_object(object)

            items.append(object)
            count += 1
            #delay param

            page = page[end_content:]
        i += 1
    return items