from multiprocessing.managers import BaseManager
from queue import Queue
import os
import sys

class Server():
    def __init__(self, ip, port, authkey, env):
        super(Server, self).__init__()
        self.ip = ip
        self.port = port
        self.authkey = authkey
        self.env = env
        self.GPU = True
        self.config = {'searchFolder': "/home/ubuntu/Data/Search/",
                       'imagesFolder': "/home/ubuntu/Data/Images/",
                       'efsImagesFolder': "/home/ubuntu/efs/Images/",
                       'serverPIDFile': "./ServerPID.txt",
                       'dn_config_file': "/home/ubuntu/efs/darknet/cfg/Charts_simple.cfg",
                       'dn_weights_file': "/home/ubuntu/efs/darknet/backup/Charts_simple_17100.weights",
                       'tesseractFolder': '/usr/share/tesseract-ocr/tessdata/',
                       'dn': "/home/ubuntu/efs/darknet/darknet",
                       'limit_main_pages': 40,
                       'limit_related_pages': 5,
                       'max_related_search': 5,
                       'suffix': 'data,graph,info,stats,statistics,chart,figures',
                       'related': True}
        if self.GPU:
            self.config['dn'] = "/home/ubuntu/efs/darknet-gpu/darknet"
            self.config['dn_config_file'] = "/home/ubuntu/efs/darknet-gpu/cfg/Charts_simple.cfg"
            self.config['dn_weights_file'] = "/home/ubuntu/efs/darknet-gpu/backup/Charts_simple_17100.weights"
        if self.env == 'PC':
            self.config['searchFolder'] = "/mnt/D/Data/Search/"
            self.config['imagesFolder'] = "/mnt/D/Data/Images/"
            self.config['efsImagesFolder'] = "/mnt/D/efs/Images/"
            self.config['tesseractFolder'] = '/usr/share/tesseract-ocr/4.00/tessdata/'
            self.config['dn'] = "/mnt/D/9raphs_project/darknet/darknet"
            self.config['dn_weights_file'] = "/mnt/D/9raphs_project/darknet/backup/Charts_simple_17100.weights"
            self.config['dn_config_file'] = "/mnt/D/9raphs_project/darknet/cfg/Charts_simple.cfg"
            self.config['limit_main_pages'] = 40
            self.config['limit_related_pages'] = 20
            self.config['max_related_search'] = 2
            self.config['suffix']  = 'Data,Graphs'
            self.config['related'] = True



    def make_server_manager(self):
        """ Create a manager for the server, listening on the given port.
            Return a manager object with all the required queues.
        """
        q_sync = Queue()  # input queue to sync manager
        q_search = Queue()  # input for the searchServer
        q_sql = Queue()  # input queue to the sqlServer
        q_download = Queue()  # queue for the output
        q_darknet = Queue()  # queue for darknet
        q_imgProc = Queue()  # queue for darknet
        BaseManager.register('q_sync', callable=lambda: q_sync)
        BaseManager.register('q_search', callable=lambda: q_search)
        BaseManager.register('q_sql', callable=lambda: q_sql)
        BaseManager.register('q_download', callable=lambda: q_download)
        BaseManager.register('q_darknet', callable=lambda: q_darknet)
        BaseManager.register('q_imgProc', callable=lambda: q_imgProc)
        BaseManager.register('config', callable=lambda: self.config)
        manager = BaseManager(address=(self.ip, self.port), authkey=self.authkey)
        print('Server started at port ' + str(self.port))
        return manager


if __name__ == "__main__":
    if len(sys.argv) > 3:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        authkey = sys.argv[3].encode('utf-8')
        env = sys.argv[4]
    else:
        ip = '127.0.0.1'
        port = 24487
        authkey = 'loko3fvppokse556'.encode('utf-8')
        env = "PC"

    s = Server(ip, port, authkey, env)
    # print(s.ip)
    with open(s.config['serverPIDFile'], 'w') as f:
        f.write(str(os.getpid()) + '\n')
    manager = s.make_server_manager()
    s = manager.get_server()
    s.serve_forever()
