from multiprocessing.managers import BaseManager
import datetime
import os
import sys

class getInput():

    def __init__(self,ip,port,authkey):
        super(getInput, self).__init__()
        self.ip = ip
        self.port = port
        self.authkey = authkey

        #Create the queus and variables
        BaseManager.register('q_sync')
        BaseManager.register('q_sql')
        BaseManager.register('q_search')
        BaseManager.register('q_download')
        BaseManager.register('q_darknet')
        BaseManager.register('q_imgProc')
        BaseManager.register('config')
        m = BaseManager(address=(ip, port), authkey=authkey)
        m.connect()
        self.q_download = m.q_download()
        self.q_sql = m.q_sql()
        self.q_sync = m.q_sync()
        self.q_search = m.q_search()
        self.q_darknet = m.q_darknet()
        self.q_imgProc = m.q_imgProc()
        self.config = m.config()._getvalue()
        self.q_sync.put(['Register',os.getpid(),'getInput'])

    def createNewSearch(self, searchString):
        '''
        Insert the search into the db
        :param searchString: the keywords that are being searched
        :return:
        '''
        #!!! Need to check if the search already exists
        #if so, need to get that search's last execution date and only "update" the search with the right arguments
        suffix = self.config['suffix']
        related = self.config['related']
        sqlstr = "INSERT INTO Search (SearchString, SearchTimeStart, SearchStatus, SearchFolderLocation, SearchRelated, SearchSuffix) " \
                 "VALUES (%s, %s, " + str(0) + ", %s," + str(related) + ', %s)'
        sqlvars = (searchString, str(datetime.datetime.now())[0:19], str(self.config['searchFolder']) + searchString + '/' , suffix)
        self.q_sql.put(['insertNewSearch',sqlstr,sqlvars,searchString, suffix])

if __name__ == "__main__":
    if len(sys.argv) == 4:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        authkey = sys.argv[3].encode('utf-8')
    else:
        ip = '172.31.6.159'
        port = 24487
        authkey = 'loko3fvppokse556'.encode('utf-8')

    getInput = getInput(ip, port, authkey)

    while True:
        x = input('What is the input? ')
        if x == 'Close':
            #let sync manage the closing
            getInput.q_sync.put(['Close'])
            break
        else:
            getInput.createNewSearch(x.strip())
