from multiprocessing.managers import BaseManager
import os
import sys
import datetime
import time
import shutil
import mysql.connector
import pandas as pd

class sqlServer():

    def MySQLDBConnect(self):
        '''
        This function establishes the connection to the AWS MySQL DB
        return: the connection
        '''
        conn = mysql.connector.connect(host="db9raphs.cexozeukxpsx.us-east-2.rds.amazonaws.com", port="3306",
                                             database="9raphs", user="jimmyg", password="6QE$QIg1i!0i")
        return conn

    def __init__(self, ip, port, authkey):
        super(sqlServer, self).__init__()
        self.ip = ip
        self.port = port
        self.authkey = authkey

        # Create the queus and variables
        BaseManager.register('q_sync')
        BaseManager.register('q_sql')
        BaseManager.register('q_search')
        BaseManager.register('q_download')
        BaseManager.register('q_darknet')
        BaseManager.register('q_imgProc')
        BaseManager.register('config')
        m = BaseManager(address=(ip, port), authkey=authkey)
        m.connect()
        self.q_download = m.q_download()
        self.q_sql = m.q_sql()
        self.q_sync = m.q_sync()
        self.q_search = m.q_search()
        self.q_darknet = m.q_darknet()
        self.q_imgProc = m.q_imgProc()
        self.config = m.config()._getvalue()
        self.q_sync.put(['Register', os.getpid(), 'sqlServer'])
        # Start the SQL connector
        self.conn = self.MySQLDBConnect()

    def sqlSelect(self, sqlstr, sqlvars):
        '''
        function that inserts into the db
        sqlstr: the string to execute
        sqlvars: the variables that need to be inserted (if relevant)
        !! The sqlvars must be a list of strings
        '''
        try:
            cursor = self.conn.cursor()
            cursor.execute(sqlstr, sqlvars)
            output = cursor.fetchall()
            rc = cursor.rowcount
            cursor.close()
        except Exception as e:
            error = 1
            output = 'DB error on Select: \n' + '   sqlstr: ' + sqlstr + '\n   sqlvars: ' + str(sqlvars) + '\n' + str(e)
            self.q_sync.put(['Error', output])
            rc = -1
        else:
            error = 0
        return error, output, rc

    def sqlInsertMany(self, sqlstr, sqlvars):
        '''
        function that updates into the db
        sqlstr: the string to execute
        sqlvars: the variables that need to be inserted (if relevant)
        !! The sqlvars must be a list of strings
        '''
        try:
            cursor = self.conn.cursor()
            cursor.executemany(sqlstr, sqlvars)
            output = cursor.lastrowid
            cursor.close()
            self.conn.commit()
        except Exception as e:
            error = 1
            output = 'DB error on insertmany: \n' + '   sqlstr: ' + sqlstr + '\n   sqlvars: ' + str(sqlvars) + '\n' + str(e)
            self.q_sync.put(['Error', output])
        else:
            error = 0
        return error, output

    def sqlInsert(self, sqlstr, sqlvars):
        '''
        function that updates into the db
        sqlstr: the string to execute
        sqlvars: the variables that need to be inserted (if relevant)
        !! The sqlvars must be a list of strings
        '''
        try:
            cursor = self.conn.cursor()
            cursor.execute(sqlstr, sqlvars)
            lastid = cursor.lastrowid
            cursor.close()
            self.conn.commit()
        except Exception as e:
            error = 1
            output = 'DB error on insert: \n' + '   sqlstr: ' + sqlstr + '\n   sqlvars: ' + str(sqlvars) + '\n' + str(e)
            self.q_sync.put(['Error', output])
        else:
            error = 0
            output = lastid
        return error, output

    def sqlUpdate(self, sqlstr, sqlvars):
        '''
        function that inserts into the db
        sqlstr: the string to execute
        sqlvars: the variables that need to be inserted (if relevant)
        !! The sqlvars must be a list of strings
        '''
        try:
            cursor = self.conn.cursor()
            cursor.execute(sqlstr, sqlvars)
            lastid = cursor.lastrowid
            cursor.close()
            self.conn.commit()
        except Exception as e:
            error = 1
            output = 'DB error on update: \n' + '   sqlstr: ' + sqlstr + '\n   sqlvars: ' + str(sqlvars) + '\n' + str(e)
            self.q_sync.put(['error',output])
        else:
            error = 0
            output = lastid
        return error, output

    def updateDarknetDone(self, rcvd):
        '''
        Function that updates the db after yolo was performed on the image
        - Inserting into the DB rows for the ImageElement and ImageDnRun table
        - Updating the Image table
        - Checking that all images of a search were performed
        :param rcvd:
            [1]: ids
            [2]: dn_output: containing the output of darknet
            [3]: weightsfile: file used for the yolov2 weights
            [4]: configfile: file used for the darknetRun
        :return:
        '''
        ids = rcvd[1]
        dn_output = rcvd[2]
        weightsFile = rcvd[3]
        configFile = rcvd[4]
        error = 0
        err = 0
        hasGraph = 0
        IElist = [] #IEList will contain the list of all the lines to be inserted in ImageElement. It is built while parsing through dnOutput

        #Get image size from the DB (used later)
        sqlstr = "Select Image.ImageWidth, Image.ImageHeight from Image where Image.idImage = %s"
        sqlvars = (ids[2],)
        _, imgSize, _ = self.sqlSelect(sqlstr, sqlvars)
        w = int(imgSize[0][0])
        h = int(imgSize[0][1])

        #Insert in DB the details of the Darknet run and the outputfile details
        timestamp = time.strftime("%Y%m%d%H%M%S")
        dnOutputfile = self.config['imagesFolder'] + str(ids[2]) + '/dn_' + str(ids[2]) + "_" + str(timestamp) + ".txt"
        f = open(dnOutputfile, 'w')
        f.write(dn_output)
        f.close()
        sqlstr = "INSERT INTO 9raphs.ImageDnRun (idImage,ImageDnRunTime,ImageDnRunConfigFile,ImageDnRunWeightsFile,ImageDnRunOutputFile) VALUES(%s,%s,%s,%s,%s)"
        sqlvars = (str(ids[2]), timestamp, configFile, weightsFile, dnOutputfile)
        error, idImageDnRun = self.sqlInsert(sqlstr,sqlvars)

        #If no error writing the DarknetRun in the DB, decompose the dn_output and store in into the DB
        if not error:
            # lines contains: best_class, names[best_class], selected_detections[i].det.prob[best_class] * 100, bbox_l, bbox_t, bbox_r, bbox_b, bbox_w, bbox_h);
            i = 0
            lines = dn_output.splitlines()
            for line in lines:
                line = line.replace(' ', '')
                line = line.replace('%', '')
                fields = line.split(",")
                if len(fields) < 7:
                    pass
                else:
                    sqlstr = "INSERT INTO 9raphs.ImageElement (idImageElement, idImageDnRun, idElement, IEProbability, IELeft, IETop, IERight, IEBottom, IEWidth, IEHeight) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                    sqlvars = (i, idImageDnRun, int(fields[0]), int(fields[2]), int(fields[3]), int(fields[4]),
                                int(fields[3]) + int(fields[5]), int(fields[4]) + int(fields[6]), int(fields[5]),
                                int(fields[6]))
                    err, _ = self.sqlInsert(sqlstr, sqlvars)

                    #DEBUG Q: If err = 1, do I still want to add the info to IELise???? Or should I use Else for the except?

                    #check if it's a graph with prob > 50
                    if int(fields[0]) == 8 and int(fields[2]) > 50:
                        hasGraph = True # the graph should not contain any text apart from labels, which we deal with separately
                        # therefore we do not add the text associated to the graph in IElist

                    left = max(0, int(fields[3]))
                    top = max(0, int(fields[4]))
                    right = min(w, int(fields[3]) + int(fields[7]))
                    bottom = min(h, int(fields[4]) + int(fields[8]))
                    width = min(w, int(fields[7]))
                    height = min(h, int(fields[8]))
                    IElist.append(
                        [i, idImageDnRun, fields[0], int(fields[2]), left, top, right, bottom, width, height])
                    i += 1
        error = error or err
        err = 0

        #If no error in wiriting the image details in the DB, update the Image table with status update and relevant info
        if not error:
            if hasGraph:
                # Get filename
                sqlstr = 'Select ImageFolder, ImageFilename from Image where idImage = %s'
                sqlvars = (str(ids[2]),)
                error, output, _ = self.sqlSelect(sqlstr,sqlvars)
                filename = output[0][0] + output[0][1]
                if not error:
                    sqlstr = 'Update Image set ImageProcessStatus = %s, ImageIsGraphML = %s, imageIsAvailable = True where idImage = %s'
                    sqlvars = ('darknetDone', 'y', ids[2])
                    err, output = self.sqlUpdate(sqlstr, sqlvars)
                    if not err:
                        self.q_imgProc.put(['OCRImage', ids, IElist, filename])
                    #DeleteEfsImage is done later in IEUpdate, so no need to add it here
            else:
                sqlstr = 'Update Image set ImageProcessStatus = %s, ImageIsGraphML = %s where idImage = %s'
                sqlvars = ('OK', 'n', ids[2])
                self.deleteImage(ids)
                self.deleteEfsImage(ids)
                self.sqlUpdate(sqlstr, sqlvars)
                # Check all images done is done here if there's no graph or in updateIE if there's a graph
                self.checkAllImagesDone(ids)

    def deleteImage(self, ids):
        sqlstr = 'Select ImageFolder, ImageFilename from Image where idImage = %s'
        sqlvars = (str(ids[2]),)
        error, output, _ = self.sqlSelect(sqlstr, sqlvars)
        filename = output[0][0] + output[0][1]
        if not error:
            if os.path.isfile(filename): os.remove(filename)
        self.checkAllImagesDone(ids)

    def deleteEfsImage(self, ids):
        errormsg = 'sqlServer - deleteEfsImage - '
        mydir = self.config['efsImagesFolder'] + str(ids[2]) + '/'
        try:
            shutil.rmtree(mydir)
        except Exception as e:
            self.q_sync.put(['Error', errormsg + str(e)])

    def checkAllRelatedSearchDone(self, ids):
        # now look if all the relatedsearches are processed.
        sqlstr = "Select RS.idRelatedSearch from RelatedSearch RS " \
                 "INNER JOIN Search S on RS.idSearch = S.idSearch " \
                 "where S.idSearch = %s and RS.RelatedSearchAllImagesProcessed = False"
        sqlvars = (str(ids[0]),)
        error, output, rc = self.sqlSelect(sqlstr,sqlvars)
        if not error:
            if rc == 0:
                # search is done!
                sqlstr = "update Search set SearchStatus = 'SearchDone', SearchTimeStop = %s where idSearch = %s"
                sqlvars = (str(datetime.datetime.now())[0:19], str(ids[0]))
                error, output = self.sqlUpdate(sqlstr, sqlvars)
        return error

    def checkAllImagesDone(self, ids):
        # Check if all images for that idrelatedsearch are updated
        #If so, it updates the status of that search in RelatedSearch and checks if all the "related searches" are processed


        sqlstr = "Select RSI.idImage from RelatedSearchImage RSI " \
                 "INNER JOIN Image I on RSI.idImage = I.idImage " \
                 "where idRelatedSearch = %s and I.ImageProcessStatus <> 'OK' and ImageDownloadStatus = 'OK'"
        sqlvars = (str(ids[1]),)
        error, output, rc = self.sqlSelect(sqlstr, sqlvars)
        if not error:
            if rc == 0:
                sqlstr = "update RelatedSearch set RelatedSearchAllImagesProcessed = True where idRelatedSearch = %s"
                sqlvars = (str(ids[1]),)
                _,_ = self.sqlUpdate(sqlstr, sqlvars)
                self.checkAllRelatedSearchDone(ids)

    def updateIE(self, rcvd):
        # ['updateIE',ids, IEWords]
        ids = rcvd[1]
        IEWords = rcvd[2]
        error = 0

        sqlstr = 'Insert into ImageElementText (idImageElement,idImageDnRun,IEText,IEConfidence) values (%s,%s,%s,%s)'
        for item in IEWords:
            sqlvars = (str(item[0]), str(item[1]), str(item[2]), str(item[3]))
            err, _ = self.sqlInsert(sqlstr, sqlvars)
            error = err or error
        if not error:
            sqlstr = 'Update Image set ImageProcessStatus = %s where idImage = %s'
            sqlvars = ('OK', ids[2])
            _, _ = self.sqlUpdate(sqlstr, sqlvars)
            sqlstr = "Select  RS.RelatedSearchString as 'SubString', I.ImageFolder as 'Folder', " \
            "I.ImageFilename as 'Fn',CE.ChartElementName as 'Element', IE.IEprobability as 'Prob', " \
            "IE.Ieleft as 'L', IE.IETop as 'T', IE.IERight as 'R',IE.IEBottom as 'B', IE.IEHeight as 'H', " \
            "IE.IEWidth as 'W', I.ImageIsGraphML as 'Is9', S.SearchString as 'String',I.ImageURL as 'URL' , " \
            "I.ImageDescription as 'Desc', I.ImageSource as 'Source' from RelatedSearchImage RSI " \
            "JOIN RelatedSearch RS ON RS.idRelatedSearch = RSI.idRelatedSearch " \
            "join Search S on RS.idSearch = S.idSearch " \
            "JOIN Image I on RSI.idImage = I.idImage " \
            "JOIN ImageDnRun IDR on I.idImage = IDR.idImage " \
            "JOIN ImageElement IE on IDR.idImageDnRun = IE.idImageDnRun " \
            "JOIN ChartElement CE on IE.idElement = CE.idChartElement " \
            "where RSI.idImage = %s and IE.idElement <> 6"
            sqlvars = (str(ids[2]),)
            err, output, rc = self.sqlSelect(sqlstr, sqlvars)
            if not err and rc > 0:
                self.q_imgProc.put(['decomposeImage',ids,output])
        else:
            sqlstr = 'Update Image set ImageProcessStatus = %s where idImage = %s'
            sqlvars = ('Some errors', ids[2])
            _, _ = self.sqlUpdate(sqlstr, sqlvars)

        self.deleteEfsImage(ids)
        self.checkAllImagesDone(ids)

    def createImageEntries(self, ids, dictImages):
        for line in dictImages:
            sqlstr = "select idImage from Image where ImageURLHash = %s"
            sqlvars = (str(line['ImageURLHash']),)
            _,_,rc = self.sqlSelect(sqlstr,sqlvars)

            #If the image does not already exist in the DB, the run the spAddImage
            if rc == 0:
                try:
                    cursor = self.conn.cursor()
                    params = [line['ImageDescription'], line['ImageFormat'], str(line['ImageHeight']),\
                             line['ImageHostDomain'], line['ImageURL'], line['ImageURLHash'],\
                             line['ImageSource'],line['ImageThumbnailURL'], str(line['ImageWidth']), \
                             line['ImageFilename'], self.config['imagesFolder'],str(ids[1]),str(0)]
                    result = cursor.callproc("cpAddImage",params)
                    cursor.close()
                    #cursor.execute("select cpAddImage_12")
                    idImage = result[12]
                except Exception as e:
                    errormsg =  'DB error on scAddImage: \n' + '   params: ' + str(sqlvars) + '\n ' + str(e)
                    self.q_sync.put(['Error', errormsg])
                #If spAddImage went well, create the folders for each image and send the download message
                else:
                    imgFolder = self.config['imagesFolder'] + str(idImage) + '/'
                    efsImgFolder = self.config['efsImagesFolder'] + str(idImage) + '/'
                    try:
                        if not (os.path.isdir(imgFolder)):
                            os.makedirs(imgFolder)
                    except Exception as e:
                        errormsg = 'DB error creating folder: \n' + '   folder: ' + imgFolder + '\n' + str(e)
                        self.q_sync.put(['Error', errormsg])
                    else:
                        try:
                            if not (os.path.isdir(efsImgFolder)):
                                os.makedirs(efsImgFolder)
                        except Exception as e:
                            errormsg = 'DB error creating efsFolder: \n' + '   efsfolder: ' + efsImgFolder + '\n' + str(e)
                            self.q_sync.put(['Error', errormsg])
                        else:
                            ids[2] = idImage
                            self.q_download.put(
                                ['downloadImage', ids, line['ImageURL'], imgFolder + line['ImageFilename'],efsImgFolder + line['ImageFilename']])

        return


    def modifyDataset (self, rcvd):
        #['modifyDataset',dataset,folder,H,W,nrColors,desc]
        idDataset = rcvd[1]
        folder = rcvd[2]
        H = rcvd[3]
        W = rcvd[4]
        nrColors = rcvd[5]
        desc = rcvd[6]
        sqlStr = "Insert into trainDataset (trainDatasetFolder,trainDatasetH,trainDatasetW," \
                 "trainDatasetNrColors,trainDatasetDesc) values (%s,%s,%s,%s,%s)"
        sqlVars = (folder,str(H),str(W),nrColors,desc)
        error, newDatasetId = self.sqlInsert(sqlStr,sqlVars)

        if not error:
            sqlStr = "SELECT ds.trainDatasetFolder, d.filename FROM 9raphs.traindata d " \
                     "join trainDataset ds on d.idtraindataSet = ds.idtrainDataset where ds.idtrainDataset = %s"
            error, output, rc = self.sqlSelect(sqlStr,(str(idDataset),))

        if not error:
            for i in output:
                sourceImage = i[0] + i[1]
                self.q_imgProc.put(['modifyImage',sourceImage,H,W,nrColors,folder,i[1],newDatasetId])



    def createDataset(self, rcvd):
        df = pd.read_csv(rcvd[1])
        df['idtraidataSet'] = '26'
        print(df.head(4))
        #df['idNr'] = df.apply(lambda row: str(row.BatchNr) + "-" + str(row.ChartNr), axis=1)
        #df['filename'] = df['ChartName']  + ".jpg"
        sqlStr = "Insert into 9raphs.traindata (filename,idtraindataSet) values (%s,%s)"

        i = 0

        while i < df.shape[0]:
            #lst = df.iloc[i:min(df.shape[0], i + 1000), 7:9].values.tolist()
            lst = df.iloc[i:min(df.shape[0], i + 1000), :].values.tolist()
            i = min(df.shape[0], i + 1000)
            self.sqlInsertMany(sqlStr,lst)

        '''
        #The code below was used only once, for a set of 100 items in lst)
        lst = df.iloc[i:min(df.shape[0], 100), 7:9].values.tolist()
        self.sqlInsertMany(sqlStr, lst)
        '''


    def insertNewSearch(self, rcvd):
        ids = [-1, -1, -1]
        sqlstr = rcvd[1]
        sqlvars = rcvd[2]
        searchString = rcvd[3]
        suffix = rcvd[4]
        error, ids[0] = self.sqlInsert(sqlstr, sqlvars)
        if not error:
            self.q_search.put(['downloadMainPages', ids, searchString, suffix])

    def insertRelatedSearch(self, rcvd):
        sqlstr = rcvd[1]
        sqlvars = rcvd[2]
        arguments = rcvd[3]
        ids = rcvd[4]
        error = False

        i = 0
        for sqlvar in sqlvars:
            error, lastid = self.sqlInsert(sqlstr, sqlvar)
            if not error:
                ids[1] = lastid
                self.q_download.put(['downloadPage', arguments[i], ids])
                i += 1
        error = 0
        if i > 0:
            sqlstr = "update Search set SearchStatus = %s where idSearch = %s"
            sqlvars = ("Downloading", str(ids[0]))
            self.sqlUpdate(sqlstr,sqlvars)

    def updateImageDownloaded(self, rcvd):
        ids = rcvd[1]
        errorDownload = rcvd[2]
        error = 0
        if errorDownload:
            self.q_sync.put(['Error', "UpdateImageDownloaded: Could not download img: " + str(ids[2])])
            sqlstr = 'Update Image set ImageDownloadStatus = %s where idImage = %s'
            sqlvars = ('Error', ids[2])
            self.sqlUpdate(sqlstr,sqlvars)
            self.deleteEfsImage(ids)
            self.checkAllImagesDone(ids)
        else:
            sqlstr = 'Update Image set ImageDownloadStatus = %s where idImage = %s'
            sqlvars = ('OK', ids[2])
            error, _ = self.sqlUpdate(sqlstr,sqlvars)
            if not error:
                sqlstr = "Select ImageFolder, ImageFilename From Image where idImage = %s"
                sqlvars = (str(ids[2]),)
                err, output, _ = self.sqlSelect(sqlstr,sqlvars)
                if not err:
                    imageFilename = output[0][0] + output[0][1]
                    # imageFilename = output[0] + '416.jpg'
                    self.q_darknet.put(['performDarknet', ids, imageFilename])

    def updateRelatedSearch(self, rcvd):
        ids = rcvd[1]
        dictImages = rcvd[2]

        sqlstr = 'Update RelatedSearch set RelatedSearchHTMLDownloaded = True where idRelatedSearch = %s'
        sqlvars = (str(ids[1]),)
        self.sqlUpdate(sqlstr,sqlvars)
        self.createImageEntries(ids, dictImages)

    def modifyImageDone (self, rcvd):
        filename = rcvd[1]
        idDataset = rcvd[2]
        sqlStr = "Insert into 9raphs.traindata (filename,idtraindataSet) values (%s,%s)"
        sqlVars = [filename,str(idDataset)]
        self.sqlInsert(sqlStr,sqlVars)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        ip = '172.31.15.90'
        port = 24487
        authkey = 'loko3fvppokse556'.encode('utf-8')
        debug = True
    else:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        authkey = sys.argv[3].encode('utf-8')
        if len(sys.argv) == 5:
            debug = True
        else:
            debug = False
    sqlS = sqlServer(ip, port, authkey)

    if debug:
        timestart = datetime.datetime.now()
        f = open('./debug/sqlServer - ' + str(os.getpid()) + '-' + str(timestart) + '.txt', 'w')


    while True:
        rcvd = sqlS.q_sql.get()
        #print(rcvd)
        if debug:
            from random import randint
            randomnr = randint(0, 100000)
            f.write('sqlServer,Start,' + str(randomnr) + ',' + str(timestart) + ',' + str(os.getpid()) + "," + str(
                rcvd) + '\n')
        if rcvd[0] == 'Close':
            if debug: f.close()
            sqlS.conn.close()
            break
        elif rcvd[0] == 'insertNewSearch':
            sqlS.insertNewSearch(rcvd)
        elif rcvd[0] == 'insertRelatedSearch':
            sqlS.insertRelatedSearch(rcvd)
        elif rcvd[0] == 'updateImageDownloaded':
            sqlS.updateImageDownloaded(rcvd)
        elif rcvd[0] == 'updateRelatedSearch':
            sqlS.updateRelatedSearch(rcvd)
        elif rcvd[0] == 'darknetDone':
            sqlS.updateDarknetDone(rcvd)
        elif rcvd[0] == 'updateIE':
            sqlS.updateIE(rcvd)
        elif rcvd[0] == 'createDataset':
            sqlS.createDataset(rcvd)
        elif rcvd[0] == 'modifyDataset':
            sqlS.modifyDataset(rcvd)
        elif rcvd[0] == 'modifyImgDone':
            sqlS.modifyImageDone(rcvd)
        if debug:
            timestop = datetime.datetime.now()
            f.write('sqlServer,Done,' + str(randomnr) + ',' + str(timestop) + ',' + str(os.getpid()) + ',' + str(
                rcvd[1]) + ',' + str(timestop - timestart) + '\n')
