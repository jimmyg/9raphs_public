from multiprocessing.managers import BaseManager
import sys
import cv2
import datetime
import os
import numpy as np

from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.patches as patches

import locale
locale.setlocale(locale.LC_ALL, 'C')
from tesserocr import PyTessBaseAPI, RIL, iterate_level


class imgProc():

    def __init__(self,ip,port,authkey):
        super(imgProc, self).__init__()
        self.ip = ip
        self.port = port
        self.authkey = authkey


        #Create the queus and variables
        BaseManager.register('q_sync')
        BaseManager.register('q_sql')
        BaseManager.register('q_search')
        BaseManager.register('q_download')
        BaseManager.register('q_darknet')
        BaseManager.register('q_imgProc')
        BaseManager.register('config')
        m = BaseManager(address=(ip, port), authkey=authkey)
        m.connect()
        self.q_download = m.q_download()
        self.q_sql = m.q_sql()
        self.q_sync = m.q_sync()
        self.q_search = m.q_search()
        self.q_darknet = m.q_darknet()
        self.q_imgProc = m.q_imgProc()
        self.config = m.config()._getvalue()
        self.q_sync.put(['Register',os.getpid(),'imgProc'])

        #self.ocr = PyTessBaseAPI(path=self.config['tesseractFolder'])

    def draw_coords(self, ax, Coords, Color):
        '''
        Used to draw the rectanlges on an img
        '''
        Rect = patches.Rectangle((Coords[0], Coords[1]), Coords[2], Coords[3], linewidth=3, edgecolor=Color,
                                 facecolor='none')
        ax.add_patch(Rect)
        return

    def decomposeImage(self,rcvd):
        '''
        This function reconstructs the graph image with boxes around the important areas
        The image is saved in the same folder as the graph itself
        :param rcvd:
            - ids
            - output of the select with all the data
        :return:
        '''

        ids = rcvd[1]
        output = rcvd[2]

        '''
        output:
        'SubString', 'Folder', 'Fn', 'Element', 'Prob', 'L', 'T', 'R', 'B', 'H', 'W', 'Is9', 'String', 'URL', 'Source'
        '''
        try:
            img = Image.open(str(output[0][1])+ str(output[0][2]))
        except Exception as e:
            self.q_sync.put(['Error',"Cannot open image for decomposition " + str(e)])
        else:
            _, ax = plt.subplots(1)
            #fig = plt.figure(frameon=False)
            ax.imshow(img)
            txt = open(str(output[0][1])+ '/txt.txt','w')
            txt.write('<p><strong>Search String</strong>: ' + output[0][12] +'\n')
            txt.write('<a href="' + output[0][13] + ' target="_blank" rel="noopener noreferrer">Image link</a>\n')
            txt.write('<a href="' + output[0][15] + ' target="_blank" rel="noopener noreferrer">Source</a>\n')
            txt.write('<strong>Description</strong>: ' + output[0][14] + '\n')
            txt.write('<strong>Elements (confidence level %) * :</strong></p>\n')
            txt.write('<p><ul>\n')

            i = 0
            while i < len(output):
                if output[i][4] > 40:
                    Coords = [max(float(output[i][5]), 0), max(float(output[i][6]), 0), float(output[i][10]), float(output[i][9])]
                    lmnt = output[0][3].replace('\n', '')
                    # print(lmnt, ": ", Coords)
                    if lmnt == 'Graph':
                        txt.write('<li><span style="color: #ff0000;">Graph (' + str(output[i][5]) + '%)</span></li>\n')
                        self.draw_coords(ax, Coords, 'r')
                    elif lmnt == 'Text':
                        txt.write('<li><span style="color: #33cccc;">Text (' + str(output[i][5]) + '%)</span></li>\n')
                        self.draw_coords(ax, Coords, 'c')
                    # elif lmnt == 'XCaption':
                    # draw_coords(ax,Coords,'g')
                    elif lmnt == 'XCoord':
                        txt.write('<li><span style="color: #000000;">X Axis (' + str(output[i][5]) + '%)</span></li>\n')
                        self.draw_coords(ax, Coords, 'k')
                    # elif lmnt == 'Y1Caption':
                    # draw_coords(ax,Coords,'y')
                    elif lmnt == 'Title':
                        txt.write('<li><span style="color: #0000ff;">Title (' + str(output[i][5]) + '%)</span></li>\n')
                        self.draw_coords(ax, Coords, 'b')
                    elif lmnt == 'Y1Coord':
                        txt.write('<li><span style="color: #000000;">Y Axis (' + str(output[i][5]) + '%)</span></li>\n')
                        self.draw_coords(ax, Coords, 'k')
                    elif lmnt == 'Legend':
                        txt.write('<li><span style="color: #008000;">Legend (' + str(output[i][5]) + '%)</span></li>\n')
                        self.draw_coords(ax, Coords, 'g')
                i += 1

            txt.write('</ul></p>\n')
            txt.write('&nbsp;\n\n*Only considering confidence levels &gt; 40%\n')
            plt.savefig(str(output[0][1]) + 'Squares.png')
            #plt.close(fig)
            txt.close()
        img.close()

    def OCRImage(self,rcvd):
        ####['splitImage', ids, IElist,filename]
        ids = rcvd[1]
        #IElist = rcvd[2]
        ####IElist contains: [i,idImageDnRun,best_class, probability,left,top,right,bottom,width,height]
        #filename = rcvd[3]
        #errormsg = 'imgProc - splitImage - '
        #level = RIL.BLOCK
        IEWords = []
        '''
        try:
            self.ocr.SetImageFile(filename)
        except Exception as e:
            self.q_sync.put(['Error', errormsg + 'opening image: ' + filename + " - error: " + str(e)])
        else:
            for item in IElist:
                try:
                    self.ocr.SetRectangle(item[4], item[5], item[8], item[9])
                    self.ocr.Recognize()
                    it = self.ocr.GetIterator()
                except Exception as e:
                    self.q_sync.put(['Error', errormsg + 'ocr iterator: ' + str(e)])
                else:
                    for r in iterate_level(it, level):
                        try:
                            word = r.GetUTF8Text(level)
                            conf = r.Confidence(level)
                        except Exception as e:
                            self.q_sync.put(['Error', errormsg + 'ocr getUTF and Conf: ' + str(e)])
                        else:
                            if conf > 40:
                                word = word.replace("\n",' ').strip()
                                IEWords.append([item[0],item[1],word,conf])
        '''
        self.q_sql.put(['updateIE',ids, IEWords])

    def modifyImage(self,rcvd):
        #rcvd= ['modifyImage',sourceImage,H,W,nrColors,folder,fn]
        sourceImg = rcvd[1]
        H = rcvd[2]
        W = rcvd[3]
        nrColors = rcvd[4]
        destFolder = rcvd[5]
        filename = rcvd[6]
        idDataset = rcvd[7]

        img = cv2.imread(sourceImg)

        ### Resize
        img = cv2.resize(img, (W, H), fx=0, fy=0, interpolation=cv2.INTER_NEAREST);

        # apply Kmeans
        # convert to the right format np.float32
        Z = img.reshape((-1, 3))
        Z = np.float32(Z)

        # define criteria, number of clusters(K) and apply kmeans()
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        _, label, center = cv2.kmeans(Z, nrColors, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
        res = center[label.flatten()]
        finalImg = res.reshape((img.shape))

        X = np.zeros((H, W), np.int8)
        Y = finalImg[:, :, 0] + finalImg[:, :, 1] * 256 + finalImg[:, :, 2] * 256 * 256
        j = 0
        for i in np.unique(Y):
            X = X | (Y == i) * j
            j += 1
        #cv2.imwrite(destFolder + filename + ".png",finalImg)
        #the code below is to save the numpy array but it's expensive (180k for 160 * 140)
        X = np.int8(X)
        np.save(destFolder + filename[:-4], X)
        #The code below is to save a np.array in csv, but that's more expensive than np.save (580kb for 160*140)
        #np.savetxt(destFolder + filename, np.asarray(X), delimiter=",")
        self.q_sql.put(['modifyImgDone',filename, idDataset])


if __name__ == "__main__":
    if len(sys.argv) < 3:
        ip = '127.0.0.1'
        port = 24487
        authkey = 'loko3fvppokse556'.encode('utf-8')
        debug = True
    else:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        authkey = sys.argv[3].encode('utf-8')
        if len(sys.argv) == 5:
            debug = True
        else: debug = False
    imgProc = imgProc(ip, port, authkey)

    if debug:
        timestart = datetime.datetime.now()
        f = open('./debug/imgProc - '  + str(os.getpid()) + '-' + str(timestart) + '.txt', 'w')

    while True:
        rcvd = imgProc.q_imgProc.get()
        if debug:
            from random import randint
            timestart = datetime.datetime.now()
            randomnr = randint(0,100000)
            f.write('imgProc,Start,' + str(randomnr) + ',' + str(timestart) + ',' + str(os.getpid()) + "," + str(rcvd) + '\n')
        if rcvd[0] == 'OCRImage':
            imgProc.OCRImage(rcvd)
        elif rcvd[0] == 'decomposeImage':
            imgProc.decomposeImage(rcvd)
        elif rcvd[0] == 'modifyImage':
            imgProc.modifyImage(rcvd)
        elif rcvd[0] == 'Close':
            if debug: f.close()
            break
        if debug:
            timestop = datetime.datetime.now()
            f.write('imgProc,Done,'  + str(randomnr) + ',' + str(timestop) + ',' + str(os.getpid()) + ',' + str(rcvd[1]) + ',' + str(timestop - timestart) + '\n')
