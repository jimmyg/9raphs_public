from multiprocessing.managers import BaseManager
from Utils import imageSearch
import os
import sys
import datetime

class searchServer():

    def __init__(self, ip, port, authkey):
        super(searchServer, self).__init__()
        self.ip = ip
        self.port = port
        self.authkey = authkey

        # Create the queus and variables
        BaseManager.register('q_sync')
        BaseManager.register('q_sql')
        BaseManager.register('q_search')
        BaseManager.register('q_download')
        BaseManager.register('q_darknet')
        BaseManager.register('q_imgProc')
        BaseManager.register('config')
        m = BaseManager(address=(ip, port), authkey=authkey)
        m.connect()
        self.q_download = m.q_download()
        self.q_sql = m.q_sql()
        self.q_sync = m.q_sync()
        self.q_search = m.q_search()
        self.q_darknet = m.q_darknet()
        self.q_imgProc = m.q_imgProc()
        self.config = m.config()._getvalue()
        self.q_sync.put(['Register', os.getpid(), 'searchServer'])

    def createArguments(self, searchString, limit, suffix):

        # build all the searchstings with the suffixes
        searchStrings = [str(searchString + " " + item).rstrip() for item in suffix.split(',')]
        arguments = []
        # get the full argument list for all the suffix + searches
        for item in searchStrings:
            arguments.append(imageSearch.createArguments(searchString, item))

        for argument in arguments:
            argument['limit'] = limit
        return arguments

    def downloadMainPages(self, rcvd):
        ids = rcvd[1]
        searchString = rcvd[2]
        suffix = rcvd[3]
        limit_main_pages = self.config['limit_main_pages']
        arguments = self.createArguments(searchString, limit_main_pages, suffix)
        sqlvars = []

        # get the first set of arguments
        for argument in arguments:

            params = imageSearch.build_url_parameters(argument)
            url = imageSearch.build_search_url(argument['keywords'], params, argument['specific_site'],
                                               argument['safe_search'])
            # create subfolder for the suffix+search
            dir = (self.config['searchFolder'] + searchString + '/' + argument['keywords'] + '/').replace(' ', '_')
            if not (os.path.isdir(dir)):
                os.makedirs(dir)
            argument['htmlfile'] = (dir + argument['keywords'] + '.html').replace(' ', '_')
            argument['jsonfile'] = (dir + argument['keywords'] + '.json').replace(' ', '_')
            argument['url'] = url
            argument['main'] = 'main'
            # add all the relatedsearch info and JSON file into the db
            sqlvars.append(
                (ids[0], argument['keywords'], argument['jsonfile'], argument['htmlfile'], argument['url'], 'Main'))
        sqlstr = "INSERT INTO RelatedSearch (idSearch,RelatedSearchString,RelatedSearchJSONFileLocation, " \
                 "RelatedSearchPageFileLocation, RelatedSearchURL,RelatedSearchMainOrRelated) VALUES (%s, %s, %s, %s, %s, %s)"
        self.q_sql.put(['insertRelatedSearch', sqlstr, sqlvars, arguments, ids])

    def downloadRelatedPage(self, rcvd):
        ids = rcvd[1]
        searchString = rcvd[2]
        htmlfile = rcvd[3]
        keywords = rcvd[4]
        limit_related_pages = self.config['limit_related_pages']
        sqlvars = []
        sqlstr = "INSERT INTO RelatedSearch (idSearch,RelatedSearchString,RelatedSearchJSONFileLocation, " \
                 "RelatedSearchPageFileLocation, RelatedSearchURL,RelatedSearchMainOrRelated) VALUES (%s, %s, %s, %s, %s, %s)"
        max_related_search = self.config['max_related_search']
        related_arguments = []
        # create the list of tabs
        with open(htmlfile, 'r') as content_file:
            content = content_file.read()
        tabs = imageSearch.get_all_tabs(content, max_related_search)

        # for each tab
        for tabskeywords, url in tabs.items():
            # correct the keywords
            related_keywords = keywords + " " + tabskeywords
            # create the argument
            related_argument = imageSearch.createArguments(searchString, related_keywords)
            related_argument['url'] = url
            dir = (self.config['searchFolder'] + searchString + '/' + keywords + '/').replace(' ', '_')
            related_argument['htmlfile'] = (dir + related_keywords + '.html').replace(' ', '_')
            related_argument['jsonfile'] = (dir + related_keywords + '.json').replace(' ', '_')
            related_argument['limit'] = limit_related_pages
            related_arguments.append(related_argument)
            sqlvars.append(
                (ids[0], related_argument['keywords'], related_argument['jsonfile'], related_argument['htmlfile'],
                 related_argument['url'], 'Related'))
        self.q_sql.put(['insertRelatedSearch', sqlstr, sqlvars, related_arguments, ids])


if __name__ == "__main__":
    if len(sys.argv) < 3:
        ip = '127.0.0.1'
        port = 24487
        authkey = 'loko3fvppokse556'.encode('utf-8')
        debug = False
    else:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        authkey = sys.argv[3].encode('utf-8')
        if len(sys.argv) == 5:
            debug = True
        else:
            debug = False
    searchServer = searchServer(ip, port, authkey)

    if debug:
        timestart = datetime.datetime.now()
        f = open('./debug/searchServer - '  + str(os.getpid()) + '-' + str(timestart) + '.txt', 'w')

    while True:
        rcvd = searchServer.q_search.get()
        if debug:
            from random import randint

            timestart = datetime.datetime.now()
            randomnr = randint(0, 100000)
            f.write('searchServer,Start,' + str(randomnr) + ',' + str(timestart) + ',' + str(os.getpid()) + "," + str(
                rcvd) + '\n')
        if rcvd[0] == 'downloadMainPages':
            searchServer.downloadMainPages(rcvd)
        if rcvd[0] == 'downloadRelatedPage':
            searchServer.downloadRelatedPage(rcvd)
        elif rcvd[0] == 'Close':
            if debug: f.close()
            break
        if debug:
            timestop = datetime.datetime.now()
            f.write('searchServer,Done,' + str(randomnr) + ',' + str(timestop) + ',' + str(os.getpid()) + ',' + str(
                rcvd[1]) + ',' + str(timestop - timestart) + '\n')
