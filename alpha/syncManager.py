from multiprocessing.managers import BaseManager
import datetime
import os
import sys
from random import randint


class syncManager():

    def __init__(self, ip, port, authkey):
        super(syncManager, self).__init__()
        self.ip = ip
        self.port = port
        self.authkey = authkey
        self.processes = []

        # Create the queus and variables
        BaseManager.register('q_sync')
        BaseManager.register('q_sql')
        BaseManager.register('q_search')
        BaseManager.register('q_download')
        BaseManager.register('q_darknet')
        BaseManager.register('q_imgProc')
        BaseManager.register('config')
        m = BaseManager(address=(ip, port), authkey=authkey)
        m.connect()
        self.q_download = m.q_download()
        self.q_sql = m.q_sql()
        self.q_sync = m.q_sync()
        self.q_search = m.q_search()
        self.q_darknet = m.q_darknet()
        self.q_imgProc = m.q_imgProc()
        self.config = m.config()._getvalue()
        self.serverPIDFile = self.config['serverPIDFile']
        self.serverPID = self.getServerPID()

    def getServerPID(self):
        try:
            with open(self.serverPIDFile, 'r') as f:
                serverPID = int(f.readline())
            return serverPID
        except Exception as e:
            print("Counldn't get Server PID: " + str(e))


if __name__ == "__main__":
    if len(sys.argv) < 3:
        ip = '127.0.0.1'
        port = 24487
        authkey = 'loko3fvppokse556'.encode('utf-8')
        debug = True
    else:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        authkey = sys.argv[3].encode('utf-8')
        if len(sys.argv) == 5:
            debug = True
        else:
            debug = False
    s_mgr = syncManager(ip, port, authkey)

    if debug:
        timestart = datetime.datetime.now()
        f = open('./debug/syncManager - '  + str(timestart) + '.txt', 'w')

    while True:
        rcvd = s_mgr.q_sync.get()
        if debug:
            from random import randint

            timestart = datetime.datetime.now()
            randomnr = randint(0, 100000)
            f.write('syncServer,Start,' + str(randomnr) + ',' + str(timestart) + ',' + str(os.getpid()) + ',' + str(
                rcvd) + '\n')
        if rcvd[0] == 'Register':
            s_mgr.processes.append([rcvd[1], rcvd[2]])
        elif rcvd[0] == 'Error':
            with open('./debug/Error.txt', 'a') as ferr:
                ferr.write(rcvd[1] + '\n')
        elif rcvd[0] == 'Close':
            if debug: f.close()
            i = 1
            for item in s_mgr.processes:
                # Issue with the 'Close' thing: 2 processes always stay open. It's not necessarily the same processes
                # Sometimes it's an sql server, sometimes a search, sometimes a download... ???

                if item[1] == 'downloadServer':
                    s_mgr.q_download.put(['Close'])
                if item[1] == 'sqlServer':
                    s_mgr.q_sql.put(['Close'])
                if item[1] == 'searchServer':
                    s_mgr.q_search.put(['Close'])
                if item[1] == 'darknetServer':
                    s_mgr.q_darknet.put(['Close'])
                if item[1] == 'imgProc':
                    s_mgr.q_imgProc.put(['Close'])

                # os.system('kill ' + str(item[0]))
            break
        else:
            pass
        if debug:
            timestop = datetime.datetime.now()
            f.write('syncServer,Done,' + str(randomnr) + ',' + str(timestop) + ',' + str(os.getpid()) + ',' + str(
                rcvd[1]) + ',' + str(timestop - timestart) + '\n')
    os.system('kill ' + str(s_mgr.serverPID))
