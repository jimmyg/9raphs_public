from multiprocessing.managers import BaseManager
import sys
import time
import os
import pexpect
import datetime

class darknetServer():

    def __init__(self,ip,port,authkey):
        super(darknetServer, self).__init__()
        self.ip = ip
        self.port = port
        self.authkey = authkey

        #Create the queus and variables
        BaseManager.register('q_sync')
        BaseManager.register('q_sql')
        BaseManager.register('q_search')
        BaseManager.register('q_download')
        BaseManager.register('q_darknet')
        BaseManager.register('q_imgProc')
        BaseManager.register('config')
        m = BaseManager(address=(ip, port), authkey=authkey)
        m.connect()
        self.q_download = m.q_download()
        self.q_sql = m.q_sql()
        self.q_sync = m.q_sync()
        self.q_search = m.q_search()
        self.q_darknet = m.q_darknet()
        self.q_imgProc = m.q_imgProc()
        self.config = m.config()._getvalue()
        self.dn_config_file = self.config['dn_config_file']
        self.dn_weights_file = self.config['dn_weights_file']
        self.dn = self.config['dn']
        self.q_sync.put(['Register',os.getpid(),'darknetServer'])
        #Load darknet
        #Use pexpect to call the darknet process
        #pexpect waits for "Enter Image Path:" line then can you send data to darknet with the "sendline" command
        self.p = pexpect.spawnu( self.dn, args = ['detector', 'test', 'cfg/Charts_simple.data', self.dn_config_file, self.dn_weights_file], cwd=os.path.dirname(self.dn), encoding='utf-8', maxread = 4000, timeout = 200)
        self.p.expect('Enter Image Path: ')

    def performDarknet(self,rcvd):
        ids = rcvd[1]
        imageFilename = rcvd[2]
        self.p.sendline(imageFilename)
        self.p.expect('Enter Image Path: ')
        dn_output = str(self.p.before)
        self.q_sql.put(['darknetDone',ids, dn_output, self.dn_weights_file, self.dn_config_file])
        pass

if __name__ == "__main__":
    if len(sys.argv) < 3:
        ip = '127.0.0.1'
        port = 24487
        authkey = 'loko3fvppokse556'.encode('utf-8')
        debug = False
    else:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        authkey = sys.argv[3].encode('utf-8')
        if len(sys.argv) == 5:
            debug = True
        else: debug = False
    darknetServer = darknetServer(ip, port, authkey)

    if debug:
        timestart = datetime.datetime.now()
        f = open('./debug/dnServer - '  + str(os.getpid()) + '-' + str(timestart) + '.txt', 'w')

    while True:
        rcvd = darknetServer.q_darknet.get()
        if debug:
            from random import randint
            timestart = datetime.datetime.now()
            randomnr = randint(0,100000)
            f.write('darknetServer,Start,' + str(randomnr) + ',' + str(timestart) + ',' + str(os.getpid()) + ',' + str(rcvd) + '\n')
        if rcvd[0] == 'performDarknet':
            darknetServer.performDarknet(rcvd)
        elif rcvd[0] == 'Close':
            if debug:
                f.close()
            break
        if debug:
            timestop = datetime.datetime.now()
            f.write('downloadServer,Done,' + str(randomnr) + ',' + str(timestop) + ',' + str(os.getpid()) + ',' + str(rcvd[1]) + ',' + str(timestop - timestart) + '\n')
